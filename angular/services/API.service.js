export class APIService {
	constructor(Restangular, ToastService) {
		'ngInject';
		//content negotiation
		let headers = {
			'Content-Type': 'application/json',
			'Accept': 'application/x.laravel.v1+json'
		};
		let token = localStorage.getItem('satellizer_token');
		if (token) {
			headers.Authorization = 'Bearer ' + token;
		}
		return Restangular.withConfig(function(RestangularConfigurer) {
			RestangularConfigurer
				.setBaseUrl('/api/')
				.setDefaultHeaders(headers)
				.setErrorInterceptor(function(response) {
					switch(response.status) {
						case 401:
							localStorage.clear();
							sessionStorage.clear();
							window.location.replace('/#/login');
							break;
						case 422:
							for (let error in response.data.errors) {
								return ToastService.error(response.data.errors[error][0]);
							}
							break;
						case 500:
							return ToastService.error(response.statusText);
						case 503:
							window.location.reload();
							break;
						default: break;
                    }
				});
		});
	}
}
