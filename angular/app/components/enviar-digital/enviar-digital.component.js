/*
	PERMISOS
	select * from permisos_hrc_web // 0.10.127.201, sa, soporte
	id	permiso
	*14	enviar a digital	ver modulo de enviar a digital para libros de archivo			x		x	x	ACTIVO	
	*15	enviar a digital	ver modulo de enviar a digital para libros de oficialia			x		x	x	ACTIVO	
	*16	enviar a digital	ver libros agragados en el area de archivo en enviar a digital	x		x	x	ACTIVO	
	*17	enviar a digital	enviar libros de archivo a digital								x		x		ACTIVO	
	*18	enviar a digital	enviar libros de oficialia a digital							x		x		NO APLICA ACTUALMENTE	los libros de oficialia no se envian a digital
*/
class UbicacionController {
	constructor($scope, $mdDialog, API, data){
		'ngInject';
		$scope.libros = data.libros;
		$scope.ubicaciones = data.ubicaciones;
		$scope.selected = [];
		$scope.cancel = () => $mdDialog.cancel();
		$scope.save = () => {
			var lib = $scope.libros.map(lib => {
					return {
						cve_oficialia: lib.cve_oficialia,	
						anio_libro: lib.anio_libro,	
						num_lomo: lib.num_lomo
					}
				});
			API.all('libro').post({ "cambiar_ubicacion": { "libros": lib, "ubicacion": $scope.ubicacion } }).then($mdDialog.hide(lib));
		}
	}
}
UbicacionController.$inject = ["$scope", "$mdDialog", "API", "data"];
class EnviarDigitalController {
	constructor($scope, API, DialogService, $filter, ToastService) {
		'ngInject';
		this.permisos = localStorage.getItem('permisos')? JSON.parse('[' + localStorage.getItem('permisos') + ']'): [];
		this.sic_oficialia = localStorage.getItem('oficialia')? JSON.parse(localStorage.getItem('oficialia')): false;
		if((this.permisos.indexOf(14) === -1 && !this.sic_oficialia) || (this.permisos.indexOf(15) === -1 && this.sic_oficialia))
		{
			localStorage.clear();
			window.location = '/#/login';
		}
		this.API = API;
		this.DialogService = DialogService;
		this.ToastService = ToastService;
		this.$filter = $filter;
		// $scope.getInventario = () => this.getInventario();
		this.writeAccess = false;
		if((this.permisos.indexOf(17) === -1 && !this.sic_oficialia) || (this.permisos.indexOf(18) === -1 && this.sic_oficialia))
			this.writeAccess = true;
		$scope.$watch(angular.bind(this, () => this.fecha_envio), () => {
			if(this.fecha_comprobante && this.fecha_envio)
				this.actualizaLibros();
		});
		$scope.$watch(angular.bind(this, () => this.fecha_comprobante), () => {
			if(this.fecha_comprobante && this.fecha_envio)
				this.actualizaLibros();
		});
		$scope.$watch(angular.bind(this, () => this.fecha_final), () => {
			if(this.fecha_final && this.deArchivo)
				this.actualizaLibros();
		});
		this.downloadURI = (uri, name) => {
			var link = document.createElement("a");
			link.download = name;
			link.href = uri;
			document.body.appendChild(link);
			link.click();
			document.body.removeChild(link);
		}
		$scope.changeBox = () => this.actualizaLibros();
	}

	$onInit() {
		this.selectedIndex = 0;
		this.deArchivo = false;
		this.fecha_envio = new Date();
		this.fecha_comprobante = new Date();
		this.fecha_final = new Date();
		this.cola = [];
		this.selected = [];
		this.selected2 = [];
		this.selected3 = [];
		//this.actualizaLibros();
	}
	actualizaLibros() {
		this.selected = [];
		this.selected2 = [];
		if(this.deArchivo)
			this.libros = this.API.all("libro/12?fecha_inicial=" + this.$filter('date')(this.fecha_comprobante, 'yyyy/MM/dd', null) + "&fecha_final=" + this.$filter('date')(this.fecha_final, 'yyyy/MM/dd', null) + (this.sic_oficialia? "&sic_oficialia=true": "")).getList().$object;
		else
			this.libros = this.API.all("libro/6?fecha_comprobante=" + this.$filter('date')(this.fecha_comprobante, 'yyyy/MM/dd', null) + (this.sic_oficialia? "&sic_oficialia=true": "")).getList().$object;
		this.enviados = this.API.all("libro/10?fecha_envio=" + this.$filter('date')(this.fecha_envio, 'yyyy/MM/dd', null) + (this.sic_oficialia? "&sic_oficialia=true": "")).getList().$object;
	}
	enviarDigital() {
		if(!this.selected || (this.selected && this.selected.length == 0)) 
			return;
		let enviar = this.selected.map(lib => {
			return {
				cve_oficialia: lib.cve_oficialia,	
				anio_libro: lib.anio_libro,
				num_lomo: lib.num_lomo
			}
		});

		if(this.sic_oficialia)
			enviar.sic_oficialia = true;

		this.API.all('libro').post({"enviar_digital": enviar}).then(() => {
			this.DialogService.alert("", "Enviado" + (this.selected.length > 1? "s":"") + " a digital", null);
			this.actualizaLibros();
		});
	}
	cambiarUbicacion() {
		if(!this.selected || (this.selected && this.selected.length == 0)) 
			return;
		let data = { 
			libros: this.selected.map(lib => {
					return {
						cve_oficialia: lib.cve_oficialia,	
						anio_libro: lib.anio_libro,	
						num_lomo: lib.num_lomo
					}
				}),
			ubicaciones: this.API.all("ubicacion_fisica").getList().$object
		};
		this.DialogService.custom(UbicacionController, "./views/dialogs/enviar-digital/cambiar_ubicacion.html", null, data)
			.then(lib => {
				this.DialogService.alert("", "Libro" + (lib.length > 1? "s":"") + " actualizado" + (lib.length > 1? "s":""), null);
				this.actualizaLibros();
			}, () => {});
	}
	imprimirPDF(libros) {
		if(!libros || (libros && libros.length == 0)) 
			return;
		var libs = libros.map(lib => {
			return {
				nom_oficialia: lib.oficialia.nom_oficialia.substring(0,35) + (lib.oficialia.nom_oficialia.length > 35? '...' : ''),
				cve_oficialia: lib.oficialia.cve_oficialia,
				tipo_libro: lib.tipo_libro.nom_tipo_libro,
				acta_inicial: lib.acta_inicial,
				acta_final: lib.acta_final,
				anio_libro: lib.anio_libro,
				num_carton: lib.num_carton,
				num_lomo: lib.num_lomo
			}
		});
		libs = libs.sort(this.dynamicSortMultiple("cve_oficialia", "tipo_libro", "acta_inicial", "acta_final"));
		var data = { libros: [], archivo: true };
		var temArr = [];
		for (let o of libs) {
			temArr.push(o);
			if(temArr.length == 50)
			{
				data.libros.push(angular.copy(temArr));
				temArr = [];
			}
		}
		if(temArr.length > 0)
			data.libros.push(angular.copy(temArr));

		this.API.all('libro').post({"comprobante": data}).then(pdfEncoded => this.downloadURI("data:application/pdf;base64," + pdfEncoded, 'comprobante' + this.$filter('date')(this.fecha_envio, 'dd-MM-yyyy', null) + '.pdf'));
	}
	dynamicSortMultiple() {
		/*
		* save the arguments object as it will be overwritten
		* note that arguments object is an array-like object
		* consisting of the names of the properties to sort by
		*/
		var props = arguments;
		return (obj1, obj2) => {
			var i = 0, result = 0, numberOfProperties = props.length;
			/* try getting a different result from 0 (equal)
			* as long as we have extra properties to compare
			*/
			while(result === 0 && i < numberOfProperties) {
				result = this.dynamicSort(props[i])(obj1, obj2);
				i++;
			}
			return result;
		}
	}
	dynamicSort(property) {
		var sortOrder = 1;
		if(property[0] === "-") {
			sortOrder = -1;
			property = property.substr(1);
		}
		return (a,b) => (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0 * sortOrder;
	}
	enviarCola() {
		var c = 0;
		for (let o of this.selected2) {
			if(this.cola.length == 0)
			{
				this.cola.push(o);
				c++;
			}
			else
			{
				if(this.cola.findIndex(i => i.cve_oficialia == o.cve_oficialia && i.anio_libro == o.anio_libro && i.num_lomo == o.num_lomo) == -1)
				{
					this.cola.push(o);
					c++;
				}
			}
		}
		this.selected2 = [];
		this.ToastService.show(c + ' libros agregados a la cola.');
	}
}
export const EnviarDigitalComponent = {
	templateUrl: './views/app/components/enviar-digital/enviar-digital.component.html',
	controller: EnviarDigitalController,
	controllerAs: 'vm',
	bindings: {}
}