class EnviarArchivoController {
	constructor($scope, API, DialogService, $filter, ToastService) {
		'ngInject';
		this.permisos = localStorage.getItem(localStorage.permisos)? JSON.parse('[' + localStorage.permisos + ']'): [];
        this.sic_oficialia = localStorage.getItem(localStorage.oficialia)? JSON.parse(localStorage.oficialia): false;
		if((this.permisos.indexOf(24) !== -1 && !this.sic_oficialia) || (this.permisos.indexOf(25) !== -1 && this.sic_oficialia))
		{
			localStorage.clear();
			window.location = '/#/login';
		}
		this.API = API;
		this.DialogService = DialogService;
		this.ToastService = ToastService;
		this.$filter = $filter;
		// $scope.getInventario = () => this.getInventario();
		this.writeAccess = false;
		if((this.permisos.indexOf(26) === -1 && !this.sic_oficialia) || (this.permisos.indexOf(27) === -1 && this.sic_oficialia))
			this.writeAccess = true;
		$scope.$watch(angular.bind(this, () => this.fecha_envio), (newVal, oldVal) => {
			if(oldVal)
				this.actualizaLibros();
		});
		this.downloadURI = (uri, name) => {
			var link = document.createElement("a");
			link.download = name;
			link.href = uri;
			document.body.appendChild(link);
			link.click();
			document.body.removeChild(link);
		}
	}

	$onInit() {
		this.selectedIndex = 0;
		this.fecha_envio = new Date();
		this.cola = [];
		this.selected = [];
		this.selected2 = [];
		this.selected3 = [];
		this.actualizaLibros();
	}
	actualizaLibros(){
		this.selected = [];
		this.selected2 = [];
		this.API.all("libro/7").getList().then(lib => {
			this.libros = lib.plain();
			if(this.libros.length == 0)
				this.selectedIndex = 1;
		});
		this.enviados = this.API.all("libro/9?fecha_envio=" + this.$filter('date')(this.fecha_envio, 'yyyy/MM/dd', null)).getList().$object;
	}
	enviarArchivo() {
		if(!this.selected || (this.selected && this.selected.length == 0)) 
			return;
		let enviar = this.selected.map(lib => {
			return {
				cve_oficialia: lib.cve_oficialia,	
				anio_libro: lib.anio_libro,
				num_lomo: lib.num_lomo
			}
		});
		this.API.all('libro').post({"enviar_archivo": enviar}).then(() => {
			this.DialogService.alert("", "Enviado" + (this.selected.length > 1? "s":"") + " a archivo", null);
			this.actualizaLibros();
		});
	}
	imprimirPDF(libros) {
		if(!libros || (libros && libros.length == 0)) 
			return;
		var libs = libros.map(lib => {
			return {
				nom_oficialia: lib.oficialia.nom_oficialia.substring(0,17) + (lib.oficialia.nom_oficialia.length > 17? '...' : ''),
				cve_oficialia: lib.oficialia.cve_oficialia,
				tipo_libro: lib.tipo_libro.nom_tipo_libro,
				acta_inicial: lib.acta_inicial,
				acta_final: lib.acta_final,
				anio_libro: lib.anio_libro,
				num_carton: lib.num_carton,
				num_lomo: lib.num_lomo
			}
		});
		libs = libs.sort(this.dynamicSortMultiple("cve_oficialia", "tipo_libro", "acta_inicial", "acta_final"));
		var data = { libros: [] };
		var temArr = [];
		for (let o of libs) {
			temArr.push(o);
			if(temArr.length == 25)
			{
				data.libros.push(angular.copy(temArr));
				temArr = [];
			}
		}
		if(temArr.length > 0)
			data.libros.push(angular.copy(temArr));

		this.API.all('libro').post({"comprobante": data}).then(pdfEncoded => this.downloadURI("data:application/pdf;base64," + pdfEncoded, 'comprobante' + this.$filter('date')(this.fecha_envio, 'dd-MM-yyyy', null)));
	}
	dynamicSortMultiple() {
		/*
		* save the arguments object as it will be overwritten
		* note that arguments object is an array-like object
		* consisting of the names of the properties to sort by
		*/
		var props = arguments;
		return (obj1, obj2) => {
			var i = 0, result = 0, numberOfProperties = props.length;
			/* try getting a different result from 0 (equal)
			* as long as we have extra properties to compare
			*/
			while(result === 0 && i < numberOfProperties) {
				result = this.dynamicSort(props[i])(obj1, obj2);
				i++;
			}
			return result;
		}
	}
	dynamicSort(property) {
		var sortOrder = 1;
		if(property[0] === "-") {
			sortOrder = -1;
			property = property.substr(1);
		}
		return (a,b) => (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0 * sortOrder;
	}
	enviarCola() {
		var c = 0;
		for (let o of this.selected2) {
			if(this.cola.length == 0)
			{
				this.cola.push(o);
				c++;
			}
			else
			{
				if(this.cola.findIndex(i => i.cve_oficialia == o.cve_oficialia && i.anio_libro == o.anio_libro && i.num_lomo == o.num_lomo) == -1)
				{
					this.cola.push(o);
					c++;
				}
			}
		}
		this.selected2 = [];
		this.ToastService.show(c + ' libros agregados a la cola.');
	}
}
export const EnviarArchivoComponent = {
	templateUrl: './views/app/components/enviar-archivo/enviar-archivo.component.html',
	controller: EnviarArchivoController,
	controllerAs: 'vm',
	bindings: {}
}