class EscanerController {
	constructor($auth, $scope, jQuery, API) {
		'ngInject';
		if(!$auth.isAuthenticated())
			window.location = '/#/login';
		this.jQuery = jQuery;
		this.API = API;
	}
	$onInit() {
		this.libros = [];
		this.oficialias = this.API.all("oficialia").getList().$object;
		this.tipos_libros = this.API.all("tipo_libro").getList().$object;
	}
	codigoNuevo() {
		var le = this.rb17(this.codigo_barras);
		if(this.libros.length > 0) {
			if(le && this.jQuery.grep(this.libros, l => l.num_lomo == le.num_lomo).length == 0)
				this.libros.push(le);
		}
		else{
			if(le)
				this.libros.push(le);
		}
		this.codigo_barras = null;
	}
	delete(index) {
		this.libros.splice(index, 1);
	}
	rb17(codigo) {
		// como se que no es rb16? como se que no es rb15 o at14?
		let pp = codigo.substr(0, 10);
		let sp = codigo.substr(10, codigo.length - 10);
		pp = parseInt(pp, 36).toString();
		let bin = parseInt(sp.substr(0, 2), 36).toString(2);
		bin = ("0000000" + bin).slice(-7);
		var numeros_libros = sp.substr(2, sp.length - 2);
		numeros_libros = parseInt(numeros_libros, 36).toString();
		numeros_libros = numeros_libros.length % 2 == 0? numeros_libros : '0' + numeros_libros;
		var libros = [];
		for(let x in bin){
			if(Number(bin.charAt(x)) != 0)
			{
				let libro = this.jQuery.grep(this.tipos_libros, t => Number(t.cve_tipo_libro) == Number(x) + 1)[0];
				if(!libro)
					return;
				libro.num_libro = numeros_libros.substr(0,2);
				libros.push(libro);
				numeros_libros = numeros_libros.substr(2, numeros_libros.length - 2);
			}
		}
		var oficialia = this.jQuery.grep(this.oficialias, o => o.cve_oficialia == pp.substr(4,5))[0];
		if(!oficialia) return;
		let libro = {
			anio_libro: pp.substr(0,4),
			num_lomo: pp.substr(9,5),
			num_tomo: pp.substr(14,2),
			oficialia: oficialia,
			libros: libros
		};
		return libro;
	}
}

export const EscanerCodigosComponent = {
	templateUrl: './views/app/components/escaner-codigos/escaner-codigos.component.html',
	controller: EscanerController,
	controllerAs: 'vm',
	bindings: {}
}