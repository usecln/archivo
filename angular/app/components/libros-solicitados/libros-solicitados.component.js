class DetallesController {

}
class LibrosSolicitadosController {
    constructor(API, ToastService, $state, DialogService) {
        'ngInject';
        this.API = API;
        this.$state = $state;
        this.ToastService = ToastService;
        this.DialogService = DialogService;
    }
    $onInit() { this.libros = this.API.all('mov_libro/1').getList().$object; }
    detalleLibros(libro) {
        this.API.all("mov_libro/2?").customGET("", {cve_empleado: libro.cve_empleado}).then(response => {
            if(response > 0)
                this.DialogService.custom(DetallesController, "./views/dialogs/inventario-libros/actas_trabajadas.html", null, libro).then(() => {}, () => {});
            else
                this.DialogService.alert("", "No hay detalle de libros", null);
        });        
    }
}
export const LibrosSolicitadosComponent = {
    templateUrl: './views/app/components/libros-solicitados/libros-solicitados.component.html',
    controller: LibrosSolicitadosController,
    controllerAs: 'vm',
    bindings: {}
}