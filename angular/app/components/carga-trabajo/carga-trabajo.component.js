/*
	PERMISOS
	select * from permisos_hrc_web // 10.10.127.201, sa, soporte
	19	liberar de digital	ver modulo liberar de digital para libros de archivo			x	x	x	ACTIVO	
	20	liberar de digital	ver modulo liberar de digital para libros de oficialia			x	x	x	ACTIVO	
	21	liberar de digital	consultar libros en liberar de digital							x	x	x	ACTIVO	
	22	liberar de digital	liberar libro de archivo en liberar de digital					x	x		ACTIVO	
	23	liberar de digital	liberar libro de oficialia en liberar de digital				x	x		NO APLICA ACTUALMENTE	los libros de oficialia no se liberan de digital
*/
class CargaTrabajoController {
	constructor($scope, API, DialogService) {
		'ngInject';
		this.API = API;
		this.DialogService = DialogService;

		this.permisos = localStorage.getItem(localStorage.permisos)? JSON.parse('[' + localStorage.permisos + ']'): [];
        this.sic_oficialia = localStorage.getItem(localStorage.oficialia)? JSON.parse(localStorage.oficialia): false;
		if((this.permisos.indexOf(19) !== -1 && !this.sic_oficialia) || (this.permisos.indexOf(20) !== -1 && this.sic_oficialia))
		{
			localStorage.clear();
			window.location = '/#/login';
		}
		if(this.sic_oficialia)
			window.location = '/#/inventario';
		$scope.getLibros = () => this.getLibros();
		this.writeAccess = false;
		if((this.permisos.indexOf(22) === -1 && !this.sic_oficialia) || (this.permisos.indexOf(23) === -1 && this.sic_oficialia))
			this.writeAccess = true;
	}

	$onInit() {
		this.busqueda = {};
		this.libros = [];
		this.selected = [];
		this.oficialias = this.API.all("oficialia").getList().$object;
		this.tipos_libros = this.API.all("tipo_libro").getList().$object;
	}

	getLibros() {
		this.selected = [];
		this.API.all("libro/8?").customGET("", this.busqueda).then(response => this.libros = response.plain());
	}

	buscar () {
		if(!this.busqueda.cve_tipo_libro)
			this.DialogService.alert("Error", "Seleccione tipo de libro", null);
		else
			this.getLibros();
	}

	liberar(libro) {
		if(!this.writeAccess) {
			this.DialogService.alert("Error", "Este usuario no tiene permiso para realizar esta acción", null);
			return;
		}
		this.DialogService.confirm("Liberar", "¿Está seguro de dar por terminado el libro por el área de digitalización?", null)
			.then(response => {
				if(response){
					this.API.all('libro').post({"liberar": libro}).then(() => {
						this.DialogService.alert("Liberado", "Liberado del area de digitalización", null);
						this.getLibros();
					});
				}
			}).catch(() => {});
	}
}
export const CargaTrabajoComponent = {
	templateUrl: './views/app/components/carga-trabajo/carga-trabajo.component.html',
	controller: CargaTrabajoController,
	controllerAs: 'vm',
	bindings: {}
}