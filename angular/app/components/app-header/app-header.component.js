class AppHeaderController{
    constructor($auth, $scope){
        'ngInject';
        this.$auth = $auth;
        this.permisos = localStorage.getItem('permisos')? JSON.parse('[' + localStorage.getItem('permisos') + ']'): [];
        this.sic_oficialia = localStorage.getItem('oficialia')? JSON.parse(localStorage.getItem('oficialia')): false;

        if($auth.isAuthenticated()) {
            this.authenticated = true;
            this.name = localStorage.name;
        }
        else
            this.authenticated = false;
        $scope.$watch(() => localStorage.satellizer_token, () => this.checkAccess());
        $scope.$watch(() => localStorage.oficialia, () => this.checkAccess());
        this.showArea = number => {
            let rule = [   
                this.permisos.indexOf(1) !== -1,
                (this.permisos.indexOf(14) !== -1 && !this.sic_oficialia) || (this.permisos.indexOf(15) !== -1 && this.sic_oficialia),
                (this.permisos.indexOf(19) !== -1 && !this.sic_oficialia) || (this.permisos.indexOf(20) !== -1 && this.sic_oficialia),
                (this.permisos.indexOf(24) !== -1 && !this.sic_oficialia) || (this.permisos.indexOf(25) !== -1 && this.sic_oficialia),
                this.permisos.indexOf(30) !== -1,
                this.permisos.indexOf(31) !== -1
            ];
            return this.authenticated && rule[Number(number)];
        }
    }

    $onInit(){
        if(localStorage.getItem('oficialia') == 'true')
            this.connection = false;
        else
        {
            localStorage.setItem('oficialia', false);
            this.connection = true; // sic de archivo
        }
    }
    checkAccess() {
        if(this.$auth.isAuthenticated())
            this.authenticated = true;
        else
            this.authenticated = false;
    }
    cambioConeccion() {
        localStorage.setItem('oficialia', !this.connection);
        window.location.replace('/#/inventario');
        window.location.reload();
    }
    logout() {
        localStorage.clear();
        sessionStorage.clear();
        window.location.replace('/#/login');
    }
    redirect(url) {
        if(url)
            window.location.replace('/#/' + url);
    }
}

export const AppHeaderComponent = {
    templateUrl: './views/app/components/app-header/app-header.component.html',
    controller: AppHeaderController,
    controllerAs: 'vm',
    bindings: {}
}
