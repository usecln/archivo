class LandingMenuController {
	constructor($auth, ToastService) {
		'ngInject';

		this.$auth = $auth;
		this.ToastService = ToastService;
	}

	$onInit(){
		this.email = '';
		this.password = '';
		window.location.replace('/#/login');
	}
	login () {
		this.ToastService.show("x");
	}
}

export const LandingMenuComponent = {
	templateUrl: './views/app/components/landing-menu/landing-menu.component.html',
	controller: LandingMenuController,
	controllerAs: 'vm',
	bindings: {}
}
