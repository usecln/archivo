class RegisterFormController {
	constructor($auth, ToastService) {
		'ngInject';
		if(!$auth.isAuthenticated())
			window.location = '/#/login';
		this.$auth = $auth;
		this.ToastService = ToastService;
	}

    $onInit(){
        this.name = '';
        this.username = '';
        this.email = '';
        this.password = '';
        this.ip = '';
        this.perfil = '';
    }

	register() {
		let user = {
			name: this.name,
			username: this.username,
			email: this.email,
			password: this.password,
			ip: this.ip,
			perfil: this.perfil
		};

		this.$auth.signup(user)
			.then(() => {
				//remove this if you require email verification
				//this.$auth.setToken(response.data);
				this.ToastService.show('Registro exitoso');
			})
			.catch(this.failedRegistration.bind(this));
	}



	failedRegistration(response) {
		if (response.status === 422) {
			for (let error in response.data.errors) {
				return this.ToastService.error(response.data.errors[error][0]);
			}
		}
		else if(response.status === 403) {
			this.ToastService.error("Este usuario no puede registrar nuevos usuarios");
		}
		else
			this.ToastService.error(response.statusText);
	}
}

export const RegisterFormComponent = {
	templateUrl: './views/app/components/register-form/register-form.component.html',
	controller: RegisterFormController,
	controllerAs: 'vm',
	bindings: {}
}
