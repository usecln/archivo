class LoginFormController {
	constructor($auth, ToastService) {
		'ngInject';
		if($auth.isAuthenticated())
			window.location = '/#/inventario';

		this.$auth = $auth;
		this.ToastService = ToastService;
	}

    $onInit(){
        this.username = '';
        this.password = '';
    }

	login() {
		let user = {
			username: this.username,
			password: this.password
		};
		this.$auth.login(user)
			.then((response) => {
				this.$auth.setToken(response.data);
				localStorage.setItem('perfil', response.data.data.user.perfil);
				localStorage.setItem('permisos', response.data.data.user.permisos);
				localStorage.setItem('area', response.data.data.user.area);
				localStorage.setItem('name', response.data.data.user.name);
				console.log(JSON.stringify(response.data.data));
				if(response.data.data.user.permisos.indexOf('31,32,33,34') != -1) { // usuarios que solo imprimen etiquetas enviarlos a ese modulo por default y en modo oficialia
					localStorage.setItem('oficialia', true);
					window.location.replace('/#/imprimir-etiquetas');
				}
				window.location.reload();
				//window.location.reload();
			})
			.catch(this.failedLogin.bind(this));
	}

	failedLogin(response) {
		if (response.status === 422) {
			for (let error in response.data.errors) {
				return this.ToastService.error(response.data.errors[error][0]);
			}
		}
		else if(response.status === 403) {
			this.ToastService.error("Este usuario no puede iniciar sesión desde este equipo");
		}
		else
			this.ToastService.error(response.statusText);
	}
}

export const LoginFormComponent = {
	templateUrl: './views/app/components/login-form/login-form.component.html',
	controller: LoginFormController,
	controllerAs: 'vm',
	bindings: {}
}
