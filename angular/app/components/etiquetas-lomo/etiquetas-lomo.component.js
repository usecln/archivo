class EtiquetasLomoController {
	constructor($auth, $scope) {
		'ngInject';
		if(!$auth.isAuthenticated())
			window.location = '/#/login';

		$scope.hojas = [];
		var st = localStorage.getItem('etiquetas');
        if(st) {
            let sotrage = JSON.parse(st);
            var size = 3;
            sotrage = sotrage.map(s => {
                s.libros = s.tipos.map(t => {
                    return {
                        nom_tipo_libro: t.nom_tipo_libro.toUpperCase(),
                        num_libro: t.num_libro
                    }
                });
                return s;
            });
            for (var i = 0; i < sotrage.length; i += size) 
                $scope.hojas.push(sotrage.slice(i, i + size));
        }
    }
}

export const EtiquetasLomoComponent = {
	templateUrl: './views/app/components/etiquetas-lomo/etiquetas-lomo.component.html',
	controller: EtiquetasLomoController,
	controllerAs: 'vm',
	bindings: {}
}
