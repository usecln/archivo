/*
	PERMISOS
	select * from permisos_hrc_web // 10.10.127.201, sa, soporte
	id	permiso
	*1	inventario	ver modulo de inventario												x	x	x	x	x	ACTIVO	todo usuario tiene este permiso, aunque no se lo ponga
	*2	inventario	consultar libros de archivo												x	x	x	x	x	ACTIVO	todo usuario tiene este permiso, aunque no se lo ponga
	*3	inventario	consultar libros de oficialia											x	x	x	x	x	ACTIVO	todo usuario tiene este permiso, aunque no se lo ponga
	*4	inventario	agregar libros de archivo												x	x	x	x		ACTIVO	
	*5	inventario	agregar libros de oficialia												x	x	x	x		ACTIVO	
	*6	inventario	editar libros de archivo de su area										x	x	x	x		ACTIVO	
	*7	inventario	editar libros de oficialia de su area									x	x	x	x		ACTIVO	
	*8	inventario	editar libros de archivo de otra area												x		ACTIVO	
	*9	inventario	editar libros de oficialia de otra area												x		ACTIVO	
	*10	inventario	eliminar libros de archivo de su area									x	x	x	x		ACTIVO	
	*11	inventario	eliminar libros de oficialia de su area									x	x	x	x		NO APLICA ACTUALMENTE	los libros de oficialia no recorren ningun area
	*12	inventario	eliminar libros de archivo de cualquier area										x		ACTIVO	
	*13	inventario	eliminar libros de oficialia de cualquier area										x		NO APLICA ACTUALMENTE	los libros de oficialia no recorren ningun area
*/
class DetalleAnosController {
	constructor($scope, $mdDialog, data){
		'ngInject';
		$scope.anos = data;
		$scope.selected = [];
		$scope.cancel = () => $mdDialog.cancel();
		$scope.select = () => $mdDialog.hide($scope.selected[0].anio_libro);
	}
}
DetalleAnosController.$inject = ["$scope", "$mdDialog", "data"];
class ModificarController {
	constructor($scope, $mdDialog, API, ToastService, data){
		'ngInject';
		$scope.saved = data.saved;
		$scope.oficialias = data.oficialias;
		$scope.tipos_libros = data.tipos_libros;
		$scope.tipos_formatos = data.tipos_formatos;
		$scope.ubicaciones = data.ubicaciones;
		$scope.estatus = data.estatus;
		$scope.condiciones = data.condiciones;
		$scope.visitador = data.visitador;
		$scope.hrc_oficialia = localStorage.getItem('oficialia')? JSON.parse(localStorage.getItem('oficialia')): false;
		if(data.add_mod)
		{
			$scope.enviar_digital = false; // no se va a digital directamente
			$scope.libro = { 
				cve_oficialia: data.cve_oficialia, 
				cve_tipo_libro: data.tipo_libro, 
				anio_libro: data.anio_libro, 
				cve_ubicacion: $scope.hrc_oficialia? 23 : (data.visitador? 1: 1), // 1: 9 
				cve_estatus_libro: $scope.hrc_oficialia? 6 : (data.visitador? 25: 3), // 25: 2
				num_tomo: 1,
				lomo: 9,
				cve_condicion: data.visitador? 14: null
			};
			// $scope.libro.cve_ubicacion = data.visitador? 1: 9;
			// $scope.libro.cve_estatus_libro = data.visitador? 25: 2;
			$scope.addMod = true;
		}
		else
		{
			if(data.view_mod)
				$scope.viewMod = true;
			else
				$scope.origin_ref = angular.copy({ cve_oficialia:data.cve_oficialia, anio_libro:data.anio_libro, num_lomo:data.num_lomo });

			if(data.cve_ubicacion == 9 && data.cve_estatus_libro == 2)
				$scope.enviar_digital = true; // si ya esta en digital poner el flag true
			$scope.libro = data;
		}
		var updateNumActas = () => {
			if(!$scope.libro) return;
			if(!$scope.editado && $scope.libro.acta_inicial && $scope.libro.acta_final) {
				if(Number($scope.libro.acta_inicial) > Number($scope.libro.acta_final)) {
					let inic = $scope.libro.acta_inicial = $scope.libro.acta_inicial.substring(0, $scope.libro.acta_final.length);
					$scope.libro.acta_inicial = Number(inic) > Number($scope.libro.acta_final)? inic.substring(0, inic.length - 1): inic;
				}
				$scope.libro.num_actas = $scope.libro.acta_final - $scope.libro.acta_inicial + 1;
				$scope.libro.num_libro = Math.ceil($scope.libro.acta_inicial / 300);
			}
			else 
				$scope.libro.num_actas = null;
			if($scope.lastinic && $scope.lastfin && $scope.libro.acta_inicial && $scope.libro.acta_final && $scope.lastinic != $scope.libro.acta_inicial && $scope.lastfin != $scope.libro.acta_final)
				$scope.editado = true;
			$scope.lastinic = $scope.libro.acta_inicial;
			$scope.lastfin = $scope.libro.acta_final;
		}
		$scope.updateNumActas = () => updateNumActas();
		$scope.$watch('libro.acta_inicial', () => updateNumActas());
		//$scope.$watch('libro.acta_final', () => updateNumActas());
		$scope.cancel = () => $mdDialog.cancel();
		$scope.save = () => {
			if(!$scope.libro.cve_tipo_formato || !$scope.libro.num_libro || !$scope.libro.num_tomo || !$scope.libro.lomo || 
				!$scope.libro.acta_inicial || !$scope.libro.acta_final || !$scope.libro.num_actas || !$scope.libro.cve_ubicacion || 
				!$scope.libro.cve_condicion || !$scope.libro.cve_estatus_libro) {
				ToastService.error("Todos los campor son requeridos");
				return;
			}
			$scope.libro.origin_ref = $scope.origin_ref;
			let obj = {"data": $scope.libro};
			if(this.hrc_oficialia)
				obj.sic_oficialia = true;
			API.all('libro').post(obj).then(response => {
				if(response == 0)
					ToastService.error("El libro ya existe en la base de datos");
				else
				{
					ToastService.show("Se guardó el libro.");
					$mdDialog.hide();
				}
			});
			$scope.saved = $scope.libro.cve_estatus_libro == 6? true: false;
		}
		$scope.changeBox = () => {
			if($scope.enviar_digital){
				// $scope.prev_ubicacion = libro.cve_ubicacion;
				$scope.prev_estatus = $scope.libro.cve_estatus_libro;
				$scope.libro.cve_ubicacion = data.visitador? 1: 1; // 1: 9 ahora siempre esta en archivo al agregarlo no se va a digital directamente
				$scope.libro.cve_estatus_libro = data.visitador? 25: 3;
			} else {
				$scope.libro.cve_ubicacion = 1;
				$scope.libro.cve_estatus_libro = $scope.prev_estatus;
			}
		}
	}
}
ModificarController.$inject = ["$scope", "$mdDialog", "API", "ToastService", "data"];
class ActasController {
	// continuar inplementando https://stackoverflow.com/questions/31240772/passing-data-to-mddialog
	constructor($scope, $mdDialog, data){
		'ngInject';
		$scope.actas = data;
		$scope.cancel = () => $mdDialog.cancel();
		$scope.select = () => $mdDialog.hide();
	}
}
ActasController.$inject = ["$scope", "$mdDialog", "data"];
class HistorialController {
	// continuar inplementando https://stackoverflow.com/questions/31240772/passing-data-to-mddialog
	constructor($scope, $mdDialog, data){
		'ngInject';
		$scope.historial = data;
		$scope.cancel = () => $mdDialog.cancel();
		$scope.select = () => $mdDialog.hide();
		$scope.millisec = fecha => Date.parse(fecha, "yyyy-MM-dd HH:mm:ss");
	}
}
HistorialController.$inject = ["$scope", "$mdDialog", "data"];
class MenuController {
	constructor() {
		this.openMenu = function($mdOpenMenu, ev) {
			$mdOpenMenu(ev);
		};
	}
	$onInit() {}
}
class InventarioLibrosController {
	constructor($scope, API, DialogService) {
		'ngInject';
		this.API = API;
		this.DialogService = DialogService;
		this.MenuController = MenuController;
		this.writeAccess = false;
		this.visitador = false;

		this.permisos = localStorage.getItem('permisos')? JSON.parse('[' + localStorage.getItem('permisos') + ']'): [];
        this.sic_oficialia = localStorage.getItem('oficialia')? JSON.parse(localStorage.getItem('oficialia')): false;
		this.validaPermiso = tipoPermiso => {
			/*
				tipoPermiso
					1 edicion
					2 eliminacion
			*/

			var libro = this.selected[0];
			if(!libro)
				return false;
			var tienePermiso = false, deSuArea = false, puedeEditarDeSuArea = false, puedeEditarDeOtraArea = false;

			if(Number(tipoPermiso) === 1) { // editar
				puedeEditarDeSuArea = (this.permisos.indexOf(6) !== -1 && !this.sic_oficialia) || (this.permisos.indexOf(7) !== -1 && this.sic_oficialia);
				puedeEditarDeOtraArea = (this.permisos.indexOf(8) !== -1 && !this.sic_oficialia) || (this.permisos.indexOf(9) !== -1 && this.sic_oficialia);
			}
			else if(Number(tipoPermiso) === 2) { // eliminar
				puedeEditarDeSuArea = (this.permisos.indexOf(10) !== -1 && !this.sic_oficialia) || (this.permisos.indexOf(11) !== -1 && this.sic_oficialia);
				puedeEditarDeOtraArea = (this.permisos.indexOf(12) !== -1 && !this.sic_oficialia) || (this.permisos.indexOf(13) !== -1 && this.sic_oficialia);
			}
			if(localStorage.area == 'archivo')
			{
				if( [1,2,3,4,5,22,23,29,99].indexOf(Number(libro.cve_ubicacion)) !== -1 )
					deSuArea = true;
			}
			else if(localStorage.area == 'digital')
			{
				if( [9,10].indexOf(Number(libro.cve_ubicacion)) !== -1 )
					deSuArea = true;
			}
			if(puedeEditarDeOtraArea || (puedeEditarDeSuArea && deSuArea))
				tienePermiso = true;
			if(!tienePermiso && (localStorage.area == 'admin' || localStorage.area == 'jefe archivo o digital'))
				tienePermiso = true;
			return tienePermiso;
		}
		this.noTienePermisoAgregar = () => !this.busqueda.cve_oficialia || !this.busqueda.tipo_libro || !this.busqueda.anio_libro || ((this.permisos.indexOf(4) === -1 && !this.sic_oficialia)) || ((this.permisos.indexOf(5) === -1 && this.sic_oficialia));
		$scope.getInventario = () => this.getInventario();
		$scope.$watch(() => localStorage.oficialia, () => {
			this.libros = [];
			this.selected = [];
			this.oficialias = this.sic_oficialia && this.oficialias_origin? this.oficialias_origin.filter(o => [6001, 6007, 6012, 6016].indexOf(Number(o.cve_oficialia)) != -1) : this.oficialias_origin;
			if([6001, 6007, 6012, 6016].indexOf(Number(this.busqueda.cve_oficialia)) === -1)
			{
				this.busqueda.cve_oficialia = null;
				this.busqueda.tipo_libro = null;
				this.busqueda.anio_libro = null;
			}
		});
		$scope.millisec = fecha => fecha? Date.parse(fecha, "yyyy-MM-dd HH:mm:ss") : null;
	}

	$onInit() {
		this.busqueda = {};
		this.libros = [];
		this.selected = [];
		this.total = 0;
		this.alert = '';
		this.API.all("oficialia").getList().then(resp => {
			this.oficialias_origin = resp.plain();
			this.oficialias = this.sic_oficialia && this.oficialias_origin? this.oficialias_origin.filter(o => [6001, 6007, 6012, 6016].indexOf(Number(o.cve_oficialia)) != -1) : this.oficialias_origin;
		});
		this.tipos_libros = this.API.all("tipo_libro").getList().$object;
		this.tipos_formatos = this.API.all("tipo_formato").getList().$object;
		this.ubicaciones = this.API.all("ubicacion_fisica").getList().$object;
		this.estatus = this.API.all("estatus").getList().$object;
		this.condiciones = this.API.all("condicion").getList().$object;
	}

	getInventario() {
		this.selected = [];
		this.busqueda.sic_oficialia = this.sic_oficialia? 'true': null;
		this.API.all("libro/2?").customGET("", this.busqueda).then(response => this.libros = response.plain());
	}

	buscar () {
		if(this.permisos.indexOf(2) === -1 && !this.sic_oficialia)
		{
			this.DialogService.alert("Error", "No tiene permiso de consultar libros de archivo", null);
			return;
		}
		if(this.permisos.indexOf(3) === -1 && this.sic_oficialia)
		{
			this.DialogService.alert("Error", "No tiene permiso de consultar libros de oficialia", null);
			return;
		}
		if(!this.busqueda.cve_oficialia || !this.busqueda.tipo_libro || !this.busqueda.anio_libro)
			this.DialogService.alert("Error", "Todos los campos son requeridos", null);
		else
			this.getInventario();
	}
	eliminar() {
		// softDelete
		this.DialogService.confirm('Confirmar', '¿Eliminar libro?', null).then(() => {
			let l = this.selected[0];
			this.API.all('libro').customDELETE("x?cve_oficialia=" + l.cve_oficialia + "&anio_libro=" + l.anio_libro + "&num_lomo=" + l.num_lomo + (this.sic_oficialia? "&sic_oficialia=true": "")).then(() => this.getInventario());
		},() => {});
	}
	modificar() {
		let l = this.selected[0];
		//if(l.ubicacion_fisica.cve_ubicacion == 1)
		{
			//if(this.visitador && l.estatus_libro.cve_estatus_libro == 25 || !this.visitador && l.estatus_libro.cve_estatus_libro != 25)
			{
				let search = { cve_oficialia: l.cve_oficialia, anio_libro: l.anio_libro, num_lomo: l.num_lomo};
				this.API.all("libro/4?").customGET("", search).then(response => {
					let libro = response.plain()[0];
					if(libro.cve_estatus_libro == 6 || libro.cve_ubicacion == 9)
						libro.saved = true;
					libro.oficialias = this.oficialias;
					libro.tipos_libros = this.tipos_libros;
					libro.tipos_formatos = this.tipos_formatos;
					libro.ubicaciones = this.ubicaciones;
					libro.estatus = this.estatus;
					libro.condiciones = this.condiciones;
					libro.visitador = this.visitador;
					this.DialogService.custom(ModificarController, "./views/dialogs/inventario-libros/modificar_libro.html", null, libro).then(() => this.getInventario(), () => {});
				});
			}
			//else
			//	this.DialogService.alert("Error", "Este usuario no puede modificar el libro seleccionado", null);
		}
		//else
		//	this.DialogService.alert("Error", "Este usuario no puede modificar el libro seleccionado", null);
	}
	detalles() {
		let l = this.selected[0];
		let search = { cve_oficialia: l.cve_oficialia, anio_libro: l.anio_libro, num_lomo: l.num_lomo, sic_oficialia: this.sic_oficialia? 'true': null };
		this.API.all("libro/4?").customGET("", search).then(response => {
			let libro = response.plain()[0];
			if(libro.cve_estatus_libro == 6 || libro.cve_ubicacion == 9)
				libro.saved = true;
			libro.view_mod = true;
			libro.oficialias = this.oficialias;
			libro.tipos_libros = this.tipos_libros;
			libro.tipos_formatos = this.tipos_formatos;
			libro.ubicaciones = this.ubicaciones;
			libro.estatus = this.estatus;
			libro.condiciones = this.condiciones;
			this.DialogService.custom(ModificarController, "./views/dialogs/inventario-libros/modificar_libro.html", null, libro).then(() => this.getInventario(), () => {});
		});
	}
	agregar() {
		let data = { 
			add_mod: true,
			cve_oficialia: this.busqueda.cve_oficialia, 
			tipo_libro: this.busqueda.tipo_libro, 
			anio_libro: this.busqueda.anio_libro,
			oficialias: this.oficialias,
			tipos_libros: this.tipos_libros,
			tipos_formatos: this.tipos_formatos,
			ubicaciones: this.ubicaciones,
			estatus: this.estatus,
			condiciones: this.condiciones,
			visitador: this.visitador
		};
		this.DialogService.custom(ModificarController, "./views/dialogs/inventario-libros/modificar_libro.html", null, data)
			.then(() => this.getInventario(), () => {});
	}
	actas() {
		let l = this.selected[0];
		let search = { cve_oficialia: l.cve_oficialia, anio_libro: l.anio_libro, num_lomo: l.num_lomo, sic_oficialia: this.sic_oficialia? 'true': null };
		this.API.all("libro/5?").customGET("", search).then(response => {
			var actas = response.plain();
			if(actas.length > 0)
				this.DialogService.custom(ActasController, "./views/dialogs/inventario-libros/actas_trabajadas.html", null, actas).then(() => this.getInventario(), () => {});
			else
				this.DialogService.alert("", "No se encontró información con estos datos", null);
		});
		
	}
	acl() { // años con libros capturados
		if(!this.busqueda.cve_oficialia || !this.busqueda.tipo_libro) return;
		let search = { cve_oficialia: this.busqueda.cve_oficialia, cve_tipo_libro: this.busqueda.tipo_libro, sic_oficialia: this.sic_oficialia? 'true': null };
		this.API.all("libro/3?").customGET("", search).then(response => {
			let anos = response.plain();
			this.DialogService.custom(DetalleAnosController, "./views/dialogs/inventario-libros/detalle_anos_capturados.html", null, anos)
				.then(anio_libro => {
					this.busqueda.anio_libro = anio_libro;
					this.getInventario();
				}, () => {});
		});
	}
	historial() {
		let l = this.selected[0];
		let search = { cve_oficialia: l.cve_oficialia, anio_libro: l.anio_libro, num_lomo: l.num_lomo, sic_oficialia: this.sic_oficialia? 'true': null };
		this.API.all("libro/11?").customGET("", search).then(response => {
			var logs = response.plain();
			if(logs.length > 0) {
				var logs_ready = [], anterior = {};
				for (let o of logs) {
					if(!anterior.cve_oficialia)
						o.diferencia = '';
					else
						o.diferencia = this.diferencia(anterior.fecha, o.fecha);
					logs_ready.push(o);
					anterior = angular.copy(o);
				}
				this.DialogService.custom(HistorialController, "./views/dialogs/inventario-libros/historial.html", null, logs_ready).then(() => this.getInventario(), () => {});
			}
			else
				this.DialogService.alert("", "No se encontró información con estos datos", null);
		});
	}
	
	diferencia(fecha1, fecha2) {
		let f1 = Date.parse(fecha1, "yyyy-MM-dd HH:mm:ss");
		let f2 = Date.parse(fecha2, "yyyy-MM-dd HH:mm:ss");
		let undia = 24 * 60 * 60 * 1000;
		let dif = Math.abs(Number(f2) - Number(f1)); //diferencia en milisegundos
		let dias = parseInt(dif/undia, 10);
		let horas = parseInt(24 * (dif/undia - dias), 10);
		let minutos = parseInt(60 * (24 * (dif/undia - dias) - horas), 10);
		let segundos = parseInt(60 * (60 * (24 * (dif/undia - dias) - horas) - minutos));
		return dias + 'd:' + horas + 'h:' + minutos + 'm:' + segundos + 's';
	}
}
export const InventarioLibrosComponent = {
	templateUrl: './views/app/components/inventario-libros/inventario-libros.component.html',
	controller: InventarioLibrosController,
	controllerAs: 'vm',
	bindings: {}
}