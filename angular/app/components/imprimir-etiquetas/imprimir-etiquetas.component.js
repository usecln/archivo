class LomoController {
	constructor($scope, $mdDialog, data) {
		'ngInject';
		$scope.lomos = data.num_lomos;
		$scope.selected = [];
		$scope.cancel = () => $mdDialog.cancel();
		$scope.save = () => $mdDialog.hide($scope.num_lomo);
	}
}
LomoController.$inject = ["$scope", "$mdDialog", "data"];
class ImprimirEtiquetasController {
	constructor($scope, API, DialogService, $filter, ToastService, jQuery) {
		'ngInject';
		///if(Number(localStorage.perfil.charAt(3)) == 0)
		//{
			//localStorage.clear();
			//window.location = '/#/login';
		//}
		
		this.API = API;
		this.DialogService = DialogService;
		this.ToastService = ToastService;
		this.$filter = $filter;
		this.writeAccess = false;
		$scope.libro = { num_tomo : 1};
		$scope.imprimir = [];
		//$scope.tipos = [];
		$scope.new = true;
		$scope.scope = {};
		
		this.permisos = localStorage.getItem('permisos')? JSON.parse('[' + localStorage.getItem('permisos') + ']'): [];
        this.sic_oficialia = localStorage.getItem('oficialia')? JSON.parse(localStorage.getItem('oficialia')): false;
        
		if(this.permisos.indexOf(31) === -1)
		{
			localStorage.clear();
			window.location = '/#/login';
		}

		$scope.printselected = function() {
			if(!$scope.libro)
				$scope.libro = { num_tomo : 1};
			$scope.libro.tipo = ''
			$scope.libro.cve_tipos = '';
			$scope.libros = [];
			if(!$scope.scope.tipos)
				return;
			var size = 2;
			for (var i = 0; i < $scope.scope.tipos.length; i += size) 
				$scope.libros.push($scope.scope.tipos.slice(i, i + size));
			for (let x = 0; x < $scope.scope.tipos.length; x ++) {
				$scope.scope.tipos[x].num_libro = null;
				$scope.libro.tipo +=  (x > 0? ', ' : '') + ($scope.scope.tipos[x].nom_tipo_libro.substr(0,3).toUpperCase() == "ADO" ? 
					"ADOP" : $scope.scope.tipos[x].nom_tipo_libro.substr(0,3).toUpperCase());
				$scope.libro.cve_tipos += (x > 0? ',' : '') + $scope.scope.tipos[x].cve_tipo_libro;
			}
		};
		
		$scope.libros = [];
		$scope.cambiar_lomo = () => $scope.cambiar_lomo_manualmente = true;
		$scope.search = () => {
		   this.DialogService.prompt("Buscar libro", "Introduzca un número", "Código de barras o número de libro", null).then(l => {
		   		this.API.all("buscar_etiqueta?numero=" + l).getList().then(resp => {
		   			let libros = resp.plain();
		   			libros = libros.map(lb => {
		   				let array_libros = lb.libros.match(/.{1,2}/g);
			   			let cve_tipos = '', num_libros = '', tipo = '', tipos = [];
			   			array_libros.map((a, i) => {
			   				if(Number(a) != 0) {
			   					cve_tipos += (i + 1) + ',';
			   					num_libros += Number(a) + ',';
			   					tipo += this.tipos_libros.filter(t => Number(t.cve_tipo_libro) === (i + 1))[0].nom_tipo_libro.substr(0,3).toUpperCase() + ',';
			   					let t = this.tipos_libros.filter(tl => Number(tl.cve_tipo_libro) === Number(i+1))[0];
			   					tipos.push({
			   						cve_tipo_libro: t.cve_tipo_libro,
			   						nom_tipo_libro: t.nom_tipo_libro,
			   						num_libro: Number(a)
			   					});
			   				}
			   			});
			   			cve_tipos = cve_tipos.charAt(cve_tipos.length-1) == ',' ? cve_tipos.substr(0, cve_tipos.length-1): cve_tipos;
			   			num_libros = num_libros.charAt(num_libros.length-1) == ',' ? num_libros.substr(0, num_libros.length-1): num_libros;
			   			tipo = tipo.charAt(tipo.length-1) == ',' ? tipo.substr(0, tipo.length-1): tipo;
		   				let lib = {
		   					codigo: lb.id,
			   				anio_libro : lb.anio_libro,
			   				cve_tipos : cve_tipos,
			   				num_libros : num_libros,
			   				num_lomo : lb.lomo,
			   				cve_oficialia: lb.cve_oficialia,
			   				oficialia : this.oficialias.filter(o => o.cve_oficialia == lb.cve_oficialia)[0],
			   				sic_oficialia : this.sic_oficialia,
			   				tipos: tipos,
			   				tipo: tipo,
			   				num_tomo: 1,
		   				}
		   				if($scope.imprimir.length == 0)
		   					$scope.imprimir.push(lib);
		   				else
		   				{
		   					let x = $scope.imprimir.filter(f => Number(f.num_lomo) === Number(lib.num_lomo));
		   					if(x.length === 0)
		   						$scope.imprimir.push(lib);	
		   				}
		   				return lib;
		   			});
					$scope.cancel();
					console.log($scope.imprimir);
		   		});
		   });
		};
		$scope.logx = () => {
			if(this.sic_oficialia) {
				if($scope.etiquetasWindow)
					$scope.etiquetasWindow.close();
				if(!$scope.new){
					$scope.new = !$scope.new;
				}
				else {
					$scope.libro.oficialia = $scope.scope.oficialia? JSON.parse($scope.scope.oficialia) : null;
					let libros = [], num_libros = '';
					$scope.libros.map(obj => { 
						obj.map(o => {
							libros.push({ cve_tipo_libro: o.cve_tipo_libro, nom_tipo_libro: o.nom_tipo_libro, num_libro: o.num_libro });
							num_libros += Number(o.num_libro) + ',';
						});
					});
					//-----------------------------------------------------
					
					var l = $scope.libro;
					l.tipos = libros;
					let cadena = (Number('' + l.anio_libro + l.oficialia.cve_oficialia + l.num_lomo + (Number(l.num_tomo) < 10? '0' + l.num_tomo: l.num_tomo)));
					var cd2 = '';
					for(let num of [1,2,3,4,5,6,7,8])
					{
						let lb = jQuery.grep(l.tipos, tx => tx.cve_tipo_libro == num);
						cd2 += lb.length == 0? '00' : (Number(lb[0].num_libro) < 10? '0' + lb[0].num_libro : lb[0].num_libro);
					}

					let librox = {
						cve_oficialia: l.oficialia.cve_oficialia,
						anio_libro: l.anio_libro,
						lomo: l.num_lomo,
						libros: cd2
					}

					console.log(librox);

					//-----------------------------------------------------
					this.API.all('guarda_etiqueta').customPOST({data: librox}, undefined, undefined, {}).then(resp => {
						resp = resp.plain();
						let final = {
							sic_oficialia: true,
							anio_libro: $scope.libro.anio_libro,
							num_lomo: $scope.libro.num_lomo,
							num_tomo: $scope.libro.num_tomo,
							tipo: $scope.libro.tipo,
							cve_tipos: $scope.libro.cve_tipos,
							tipos: libros,
							num_libros: num_libros,
							oficialia: $scope.libro.oficialia,
							codigo: resp.id
						}
						this.API.one("libro?generate_barcode=true&code=" + final.codigo).post().then(() => {
							if($scope.new) {
								if($scope.libro.$$hashKey) {
									let x = $scope.imprimir.filter(li => li.$$hashKey == $scope.libro.$$hashKey);
									x[0] = angular.copy(final);
									$scope.libro = { num_tomo : 1 };
									$scope.scope.oficialia = null;
									$scope.scope.tipos = [];
									$scope.libros = [];
									$scope.new = false;
									console.log($scope.imprimir);
								}
								else 
								{
									$scope.imprimir.push(angular.copy(final));
									$scope.libro = { num_tomo : 1 };
									$scope.scope.oficialia = null;
									$scope.scope.tipos = [];
									$scope.libros = [];
									$scope.new = false;
									console.log($scope.imprimir);
								}
							}
						});
					});
				}
			}
			else {
				if($scope.etiquetasWindow)
					$scope.etiquetasWindow.close();
				if(!$scope.new){
					$scope.new = !$scope.new;
				}
				else {
					if(!$scope.libro)
					{
						this.DialogService.alert("Error", "Todos los campos son requeridos", null);
						return;
					}
					$scope.incomplete = false;
					$scope.libro.num_libros = '';
					var tipos = $scope.scope.tipos? $scope.scope.tipos.map(obj => {
						if(!obj.num_libro)
							$scope.incomplete = true;
						$scope.libro.num_libros += obj.num_libro + ',';
						return {
							cve_tipo_libro: obj.cve_tipo_libro,
							nom_tipo_libro: obj.nom_tipo_libro,
							num_libro: obj.num_libro
						};
					}) : [];
					$scope.libro.tipos = tipos;
					$scope.libro.oficialia = $scope.scope.oficialia? JSON.parse($scope.scope.oficialia) : null;
					l = $scope.libro;
					if(!l.anio_libro || !l.num_tomo || !l.tipo || (l.tipo && l.tipo.length == 0 || !l.oficialia))
						$scope.incomplete = true;
					if($scope.incomplete)
						this.DialogService.alert("Error", "Todos los campos son requeridos", null);
					else
					{
						// validar que exista en el hrc
						this.ToastService.show("Obteniendo número de lomo");
						this.API.one("libro?validate_exists=true&cve_oficialia=" + l.oficialia.cve_oficialia + "&anio_libro=" + l.anio_libro + 
							"&num_tomo=" + l.num_tomo + "&cve_tipo_libro=" + l.cve_tipos + "&num_libro=" + l.num_libros
							).post().then(resp => {
								resp = resp.plain();
								if(resp.length < l.cve_tipos.split(",").length) {
									if(l.tipos.length > 1) {
										let tipos_solicitados = l.cve_tipos.split(",").filter(yy => jQuery.grep(resp, xx => xx.cve_tipo_libro == yy).length == 0);
										let tipos_faltantes = '';
										var arreglo_faltante = this.tipos_libros.filter(tl => jQuery.grep(tipos_solicitados, xx => xx == tl.cve_tipo_libro).length != 0);
										for(let key in arreglo_faltante)
											tipos_faltantes += arreglo_faltante[key].nom_tipo_libro + (key == arreglo_faltante.length - 1? '' : ', ');
										this.DialogService.alert('Error', 'El libro mixto indicado no existe en el sistema: ' + 
											(arreglo_faltante.length == 1? 'No existe libro de ' + arreglo_faltante[0].nom_tipo_libro : 'No existen libros de: ' + tipos_faltantes));
									}
									else
										this.DialogService.alert("Error", "El libro de " + l.tipos[0].nom_tipo_libro + " indicado no existe en el sistema");
									return;
								}
								else {
									// checar si alguno de los libros tiene numero de lomo, 
									// si mas de uno tiene preguntar cual quiere elegir,
									// si ninguno tiene asignar next_lomo
									let lomos = resp.map(r => r.lomo).filter(f => Number(f) != 9);
									lomos.push(resp[0].next_lomo);
									lomos = lomos.filter( (value, index, self) => self.indexOf(value) === index );
									if([1,2].indexOf(lomos.length) != -1) {
										if(!$scope.cambiar_lomo_manualmente){
											$scope.libro.num_lomo = lomos[0];
										}
										lomos[0] = $scope.libro.num_lomo;

										// generar cadena para codigo de barras
										let cadena = (Number('' + l.anio_libro + l.oficialia.cve_oficialia + l.num_lomo + (Number(l.num_tomo) < 10? '0' + l.num_tomo: l.num_tomo))).toString(36);
										var cd2 = '';
										var bin = '';
										for(let num of [1,2,3,4,5,6,7,8])
										{
											let lb = jQuery.grep(l.tipos, tx => tx.cve_tipo_libro == num);
											bin += lb.length == 0? '0' : '1';
											cd2 += lb.length == 0? '' : (Number(lb[0].num_libro) < 10? '0' + lb[0].num_libro : lb[0].num_libro);
										}
										let b36 = parseInt(bin, 2).toString(36);
										b36 = b36.length < 2? '0' + b36: b36;
										cd2 = (Number(cd2)).toString(36);
										cd2 = cd2.length % 2 == 0? cd2 : '0' + cd2;
										cadena += b36 + cd2;
										l.codigo = cadena.toUpperCase();
										// generar cadena para codigo de barras
										this.ToastService.show("Actualizando número de lomo");
										this.API.one("libro?actualiza_lomos=true&cve_oficialia=" + l.oficialia.cve_oficialia + "&anio_libro=" + l.anio_libro + "&cve_tipo_libro="
											+ l.cve_tipos + "&num_libro=" + l.num_libros + "&num_lomo=" + lomos[0]).post().then(() => {
											this.ToastService.show("Generando código de barras");
											this.API.one("libro?generate_barcode=true&code=" + l.codigo).post().then(() => {
												$scope.cambiar_lomo_manualmente = false;
												if(!$scope.libro.$$hashKey)
													$scope.imprimir.push(angular.copy($scope.libro));
												$scope.libro = { num_tomo : 1};
												$scope.scope.oficialia = null;
												$scope.scope.tipos = [];
												$scope.libros = [];
												$scope.new = false;
											}, () => {});
										}, () => {});
									}
									else{
										let data = { 
											num_lomos: lomos
										};
										this.DialogService.custom(LomoController, "./views/dialogs/imprimir-etiquetas/seleccionar_lomo.html", null, data)
											.then(lom => {
												$scope.libro.num_lomo = lom; // mostrar dialog para seleccionar un lomo del arreglo lomos

												// generar cadena para codigo de barras
												let cadena = (Number('' + l.anio_libro + l.oficialia.cve_oficialia + l.num_lomo + (Number(l.num_tomo) < 10? '0' + l.num_tomo: l.num_tomo))).toString(36);
												var cd2 = '';
												var bin = '';
												for(let num of [1,2,3,4,5,6,7,8])
												{
													let lb = jQuery.grep(l.tipos, tx => tx.cve_tipo_libro == num);
													bin += lb.length == 0? '0' : '1';
													cd2 += lb.length == 0? '' : (Number(lb[0].num_libro) < 10? '0' + lb[0].num_libro : lb[0].num_libro);
												}
												let b36 = parseInt(bin, 2).toString(36);
												b36 = b36.length < 2? '0' + b36: b36;
												cd2 = (Number(cd2)).toString(36);
												cd2 = cd2.length % 2 == 0? cd2 : '0' + cd2;
												cadena += b36 + cd2;
												l.codigo = cadena.toUpperCase();
												// generar cadena para codigo de barras
												this.ToastService.show("Actualizando número de lomo");
												this.API.one("libro?actualiza_lomos=true&cve_oficialia=" + l.oficialia.cve_oficialia + "&anio_libro=" + l.anio_libro + "&cve_tipo_libro="
													+ l.cve_tipos + "&num_libro=" + l.num_libros + "&num_lomo=" + lom).post().then(() => {
													this.ToastService.show("Generando código de barras");
													this.API.one("libro?generate_barcode=true&code=" + l.codigo).post().then(() => {
														$scope.cambiar_lomo_manualmente = false;
														if(!$scope.libro.$$hashKey)
															$scope.imprimir.push(angular.copy($scope.libro));
														$scope.libro = { num_tomo : 1};
														$scope.scope.oficialia = null;
														$scope.scope.tipos = [];
														$scope.libros = [];
														$scope.new = false;
													}, () => {});
												}, () => {});
											}, () => {});
									}
								}
								//$scope.enviarA.$setPristine();
								//$scope.enviarA.$setUntouched();
							}, () => {});
					}
				}
				localStorage.setItem('etiquetas', JSON.stringify($scope.imprimir));
			}
		}
		$scope.print = () => {
			if($scope.etiquetasWindow)
				$scope.etiquetasWindow.close();
			localStorage.setItem('etiquetas', JSON.stringify($scope.imprimir));
			$scope.etiquetasWindow = window.open('/#/etiquetas-lomo-nuevas','winname','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=1200,height=700');
		}
		$scope.printfront = () => {
			if($scope.etiquetasWindow)
				$scope.etiquetasWindow.close();
			localStorage.setItem('etiquetas_frente', JSON.stringify($scope.imprimir));
			$scope.etiquetasWindow = window.open('/#/etiquetas-frente','winname','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=1200,height=700');
		}
		$scope.edit = libro => {
			$scope.libro = libro;
			$scope.new = true;
			$scope.scope.oficialia = JSON.stringify(jQuery.grep(this.oficialias, lb => lb.cve_oficialia == libro.oficialia.cve_oficialia)[0]);
			$scope.scope.tipos = libro.tipos;
			
			/*
			for (let obj of libro.tipos) {
				var lbs = jQuery.grep(this.tipos_libros, tl => Number(tl.cve_tipo_libro) == Number(obj.cve_tipo_libro));
				if(lbs.length == 1)
					$scope.scope.tipos.push(lbs[0]);
			}
			*/
			$scope.libros = [];
			var size = 2;
			for (var i = 0; i < $scope.scope.tipos.length; i += size) 
				$scope.libros.push($scope.scope.tipos.slice(i, i + size));
			for (let x = 0; x < $scope.scope.tipos.length; x ++) 
				$scope.scope.tipos[x].num_libro = jQuery.grep(libro.tipos, lt => lt.cve_tipo_libro == $scope.scope.tipos[x].cve_tipo_libro)[0].num_libro;
		};
		$scope.cancel = () => {
			$scope.libro = { num_tomo : 1};
			$scope.scope.oficialia = null;
			$scope.scope.tipos = [];
			$scope.libros = [];
			$scope.new = false;
		};
		$scope.delete = index => {
			$scope.imprimir.splice(index, 1);
			if($scope.imprimir.length == 0)
				$scope.new = true;
		}
	}

	$onInit() {
		this.selectedIndex = 0;		
		this.API.all("oficialia").getList().then(resp => {
			this.oficialias_origin = resp.plain();
			this.oficialias = this.sic_oficialia && this.oficialias_origin? this.oficialias_origin.filter(o => [6001, 6007, 6012, 6016].indexOf(Number(o.cve_oficialia)) != -1) : this.oficialias_origin;
		});
		this.API.all("tipo_libro").getList().then(rsp => {
			this.tipos_libros = rsp.plain();
			this.tipos_libros = rsp.plain();
		});
	}
	ImprimirEtiquetas() {
		
	}
}
export const ImprimirEtiquetasComponent = {
	templateUrl: './views/app/components/imprimir-etiquetas/imprimir-etiquetas.component.html',
	controller: ImprimirEtiquetasController,
	controllerAs: 'vm',
	bindings: {}
}