class EtiquetasFrenteController {
	constructor($auth, $scope) {
		'ngInject';
		if(!$auth.isAuthenticated())
			window.location = '/#/login';
		$scope.hojas = [];
		var st = localStorage.getItem('etiquetas_frente');
		var lb = JSON.parse(st);
		var storage = [], etiquetas = [];
		var buscarlibro = (array, numLibro) => {
			for(var i = 0; i < array.length; i++) {
				if(array[i] == numLibro)
					return true;
			}
			return false;
		}

		if(st) {
			//dividir
			lb.map(s => {
				var numeroLibros = [];
				for(var i = 0; i < s.tipos.length; i++) {
					var etiqueta = angular.copy(s);
					etiqueta.tipo = null;
					var subLibrosEtiquetas = [];
					if(!buscarlibro(numeroLibros,s.tipos[i].num_libro)) {
						subLibrosEtiquetas.push(s.tipos[i]);
						numeroLibros.push(s.tipos[i].num_libro);
						for(var j = i + 1; j < s.tipos.length; j++) {
							if(s.tipos[i].num_libro == s.tipos[j].num_libro) {
								subLibrosEtiquetas.push(s.tipos[j]);
							}
						}
						etiqueta.tipos = subLibrosEtiquetas;
						etiquetas.push(etiqueta);
					}
				}
			});
			//
			//mappear
			var size = 8;
			storage = etiquetas;
			storage = storage.map(s => {
				s.libros = s.tipos.map(t => {
					return {
						nom_tipo_libro: t.nom_tipo_libro.toUpperCase(),
						num_libro: t.num_libro
					}
				});
				return s;
			});
			//
			//separar (8 por hoja)
			for (var k = 0; k < storage.length; k += size) 
				$scope.hojas.push(storage.slice(k, k + size));
			//
		}
	}
}

export const EtiquetasFrenteComponent = {
	templateUrl: './views/app/components/etiquetas-frente/etiquetas-frente.component.html',
	controller: EtiquetasFrenteController,
	controllerAs: 'vm',
	bindings: {}
}