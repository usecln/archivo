class TrabajoPendienteController {
	constructor($scope, API, DialogService) {
		'ngInject';
		this.API = API;
		this.DialogService = DialogService;
		this.permisos = localStorage.getItem('permisos')? JSON.parse('[' + localStorage.getItem('permisos') + ']'): [];
        this.sic_oficialia = localStorage.getItem('oficialia')? JSON.parse(localStorage.getItem('oficialia')): false;

		if(this.permisos.indexOf(30) === -1)
		{
			localStorage.clear();
			window.location = '/#/login';
		}
		else
			this.writeAccess = true;
		$scope.getLibros = () => this.cambioArea();
	}

	$onInit() {
		this.libros = [];
		this.selected = [];
		this.areas = this.API.all("area").getList().$object;
	}

	cambioArea() {
		this.selected = [];
		this.libros = this.API.all("libro/13?area=" + this.area).getList().$object;
	}
}
export const TrabajoPendienteComponent = {
	templateUrl: './views/app/components/trabajo-pendiente/trabajo-pendiente.component.html',
	controller: TrabajoPendienteController,
	controllerAs: 'vm',
	bindings: {}
}