export function LimitToMax() {
	return {
		link: function(scope, element, attributes) {
			element.on("keydown keyup", function(e) {
				if (Number(element.val()) > Number(attributes.max) && e.keyCode != 46 && e.keyCode != 8 ) { // 46 delete, 8 backspace
					e.preventDefault();
					let old = String(element.val()).substring(0, attributes.max.length);
					old = Number(old) > Number(attributes.max)? old.substring(0, old.length - 1): old;
					element.val(old);
				}
			});
		}
	}
}