angular.module('app.factories')
	.factory('jQuery', [
        '$window',
        function ($window) {
            return $window.jQuery;
        }
    ]);