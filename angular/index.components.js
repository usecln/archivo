import {AppHeaderComponent} from './app/components/app-header/app-header.component';
import {AppRootComponent} from './app/components/app-root/app-root.component';
import {AppShellComponent} from './app/components/app-shell/app-shell.component';
import {ResetPasswordComponent} from './app/components/reset-password/reset-password.component';
import {ForgotPasswordComponent} from './app/components/forgot-password/forgot-password.component';
import {LoginFormComponent} from './app/components/login-form/login-form.component';
import {LandingMenuComponent} from './app/components/landing-menu/landing-menu.component';
import {InventarioLibrosComponent} from './app/components/inventario-libros/inventario-libros.component';
import {RegisterFormComponent} from './app/components/register-form/register-form.component';
import {LibrosSolicitadosComponent} from './app/components/libros-solicitados/libros-solicitados.component';
import {EnviarDigitalComponent} from './app/components/enviar-digital/enviar-digital.component';
import {EnviarArchivoComponent} from './app/components/enviar-archivo/enviar-archivo.component';
import {CargaTrabajoComponent} from './app/components/carga-trabajo/carga-trabajo.component';
import {TrabajoPendienteComponent} from './app/components/trabajo-pendiente/trabajo-pendiente.component';
import {EtiquetasFrenteComponent} from './app/components/etiquetas-frente/etiquetas-frente.component';
import {EtiquetasLomoComponent} from './app/components/etiquetas-lomo/etiquetas-lomo.component';
import {EtiquetasLomoNuevasComponent} from './app/components/etiquetas-lomo-nuevas/etiquetas-lomo-nuevas.component';
import {EscanerCodigosComponent} from './app/components/escaner-codigos/escaner-codigos.component';
import {ImprimirEtiquetasComponent} from './app/components/imprimir-etiquetas/imprimir-etiquetas.component';

angular.module('app.components')
	.component('appHeader', AppHeaderComponent)
	.component('appRoot', AppRootComponent)
	.component('appShell', AppShellComponent)
	.component('resetPassword', ResetPasswordComponent)
	.component('forgotPassword', ForgotPasswordComponent)
	.component('loginForm', LoginFormComponent)
	.component('landingMenu', LandingMenuComponent)
	.component('inventarioLibros', InventarioLibrosComponent)
	.component('registerForm', RegisterFormComponent)
	.component('enviarDigital', EnviarDigitalComponent)
	.component('enviarArchivo', EnviarArchivoComponent)
	.component('librosSolicitados', LibrosSolicitadosComponent)
	.component('cargaTrabajo', CargaTrabajoComponent)
	.component('trabajoPendiente', TrabajoPendienteComponent)
	.component('etiquetasFrente', EtiquetasFrenteComponent)
	.component('etiquetasLomo', EtiquetasLomoComponent)
	.component('etiquetasLomoNuevas', EtiquetasLomoNuevasComponent)
	.component('escanerCodigos', EscanerCodigosComponent)
	.component('imprimirEtiquetas', ImprimirEtiquetasComponent);