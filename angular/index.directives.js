import {RestrictTo} from './directives/restrict_to.directive';
import {RestrictLength} from './directives/restrict_length.directive';
import {LimitToMax} from './directives/limit_to_max.directive';

angular.module('app.directives')
	.directive('restrictTo', RestrictTo)
	.directive('restrictLength', RestrictLength)
	.directive('limitToMax', LimitToMax);