export function IconSetConfig($mdIconProvider) {
	'ngInject';
	$mdIconProvider.defaultIconSet('img/icons/sets/core-icons.svg', 24);
}