export function RoutesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
	'ngInject';

	let getView = (viewName) => {
		return `./views/app/pages/${viewName}/${viewName}.page.html`;
	};

    $locationProvider.hashPrefix('');
	$urlRouterProvider.otherwise('/');

    /*
        data: {auth: true} would require JWT auth
        However you can't apply it to the abstract state
        or landing state because you'll enter a redirect loop
    */

	$stateProvider
		.state('app', {
			abstract: true,
            data: {},
			views: {
				header: {
					templateUrl: getView('header')
				},
				footer: {
					templateUrl: getView('footer')
				},
				main: {}
			}
		})
		.state('app.landing', {
            url: '/',
            views: {
                'main@': {
                    templateUrl: getView('landing')
                }
            }
        })
        .state('app.inventario', {
            url: '/inventario',
            data: {auth: true},
            views: {
                'main@': {
                    templateUrl: getView('inventario')
                }
            }
        })
        .state('app.libros_solicitados', {
            url: '/libros-solicitados',
            views: {
                'main@': {
                    templateUrl: getView('libros-solicitados')
                }
            }
        })
        .state('app.mantenimiento', {
            url: '/mantenimiento',
            data: {auth: true},
            views: {
                'main@': {
                    templateUrl: getView('mantenimiento')
                }
            }
        })
        .state('app.enviar_digital', {
            url: '/enviar-digital',
            data: {auth: true},
            views: {
                'main@': {
                    templateUrl: getView('enviar-digital')
                }
            }
        })
        .state('app.enviar_archivo', {
            url: '/enviar-archivo',
            data: {auth: true},
            views: {
                'main@': {
                    templateUrl: getView('enviar-archivo')
                }
            }
        })
        .state('app.carga-trabajo', {
            url: '/carga-trabajo',
            data: {auth: true},
            views: {
                'main@': {
                    templateUrl: getView('carga-trabajo')
                }
            }
        })
        .state('app.login', {
			url: '/login',
			views: {
				'main@': {
					templateUrl: getView('login')
				}
			}
		})
        .state('app.register', {
            url: '/register',
            data: {auth: true},
            views: {
                'main@': {
                    templateUrl: getView('register')
                }
            }
        })
        .state('app.forgot_password', {
            url: '/forgot-password',
            views: {
                'main@': {
                    templateUrl: getView('forgot-password')
                }
            }
        })
        .state('app.trabajo_pendiente', {
            url: '/trabajo-pendiente',
            views: {
                'main@': {
                    templateUrl: getView('trabajo-pendiente')
                }
            }
        })
        .state('app.reset_password', {
            url: '/reset-password/:email/:token',
            views: {
                'main@': {
                    templateUrl: getView('reset-password')
                }
            }
        }).state('app.imprimir-etiquetas', {
            url: '/imprimir-etiquetas',
            views: {
                'main@': {
                    templateUrl: getView('imprimir-etiquetas')
                }
            }
        }).state('app.escaner', {
            url: '/escaner',
            views: {
                'main@': {
                    templateUrl: getView('escaner')
                }
            }
        }).state('etiquetas_frente', {
            url: '/etiquetas-frente',
            views: {
                'main@': {
                    templateUrl: getView('etiquetas-frente')
                }
            }
        }).state('etiquetas_lomo', {
            url: '/etiquetas-lomo',
            views: {
                'main@': {
                    templateUrl: getView('etiquetas-lomo')
                }
            }
        }).state('etiquetas_lomo_nuevas', {
            url: '/etiquetas-lomo-nuevas',
            views: {
                'main@': {
                    templateUrl: getView('etiquetas-lomo-nuevas')
                }
            }
        });
}