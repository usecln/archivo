<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('auth/login', 'Auth\AuthController@login');
Route::post('auth/register', 'Auth\AuthController@register');

Route::post('auth/password/email', 'Auth\PasswordResetController@sendResetLinkEmail');
Route::get('auth/password/verify', 'Auth\PasswordResetController@verify');
Route::post('auth/password/reset', 'Auth\PasswordResetController@reset');


//protected API routes with JWT (must be logged in)
Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
Route::resource('oficialia', 'OficialiaController', ['only' => ['index','show']]);
Route::resource('area', 'AreaController', ['only' => ['index','show']]);
Route::resource('tipo_libro', 'TipoLibroController', ['only' => ['index','show']]);
Route::resource('libro', 'LibroController', ['only' => ['show', 'store', 'destroy']]);
Route::resource('ubicacion_fisica', 'UbicacionFisicaController', ['only' => ['index','show']]);
Route::resource('condicion', 'CondicionController', ['only' => ['index','show']]);
Route::resource('estatus', 'EstatusController', ['only' => ['index','show']]);
Route::resource('tipo_formato', 'TipoFormatoController', ['only' => ['index','show']]);
Route::resource('mov_libro', 'MovLibroController', ['only' => ['index','show']]);

Route::post('test', 'EtiquetasController@etiquetas');
Route::post('guarda_etiqueta', 'EtiquetasController@guarda_etiqueta');
Route::get('buscar_etiqueta', 'EtiquetasController@buscar_etiqueta');
Route::get('bcrypt/{val}', function (Request $request, $val) {
	return bcrypt($val);
});

Route::get('token_decode', function (Request $request) {
    try {
        if(!$user = JWTAuth::parseToken()->toUser()) 
            return response()->json(null, 422);
    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
        return response()->json(null, 422);
    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
        return response()->json(null, 422);
    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
        return response()->json(null, 422);
    }

    // the token is valid and we have found the user via the sub claim
    return response()->json(compact('user'));
});