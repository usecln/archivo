<?php

namespace HRC;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Libro extends Model
{
    use SoftDeletes;
	//protected $table = 'libros';
    protected $connection = 'archivo';
    protected $fillable = [
    	'cve_oficialia', 'anio_libro', 'num_lomo', 'cve_condicion', 'cve_ubicacion',
    	'cve_tipo_formato', 'cve_tipo_libro', 'cve_estatus_libro', 'num_libro', 'num_tomo', 'num_actas', 
    	'acta_inicial', 'acta_final', 'lomo', 'flag', 'cve_ubicacion1', 'rango', 'ok', 'created_at', 'updated_at', 'deleted_at'
    	]; 
    protected $hidden = [];
    protected $dates = ['deleted_at'];
    //protected $primaryKey = ['cve_oficialia', 'anio_libro', 'num_lomo'];
    public function condicion()
    {        
        return $this->hasOne('HRC\Condicion', 'cve_condicion', 'cve_condicion');
    }
    public function ubicacion_fisica()
    {        
        return $this->hasOne('HRC\UbicacionFisica', 'cve_ubicacion', 'cve_ubicacion');
    }
    public function tipo_formato()
    {        
        return $this->hasOne('HRC\TipoFormato', 'cve_tipo_formato', 'cve_tipo_formato');
    }
    public function tipo_libro()
    {        
        return $this->hasOne('HRC\TipoLibro', 'cve_tipo_libro', 'cve_tipo_libro');
    }
    public function estatus_libro()
    {        
        return $this->hasOne('HRC\EstatusLibro', 'cve_estatus_libro', 'cve_estatus_libro');
    }
    public function oficialia()
    {
        return $this->hasOne('HRC\Oficialia', 'cve_oficialia', 'cve_oficialia');
    }
    public function changeConnection($conn)
    {
        $this->connection = $conn;
    }
}