<?php

namespace HRC;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Area extends Model
{
	use SoftDeletes;

	protected $table = 'areas';
    protected $connection = 'archivo';
    protected $fillable = [
        'id', 'area'
    ];

    protected $hidden = [

    ];
}
