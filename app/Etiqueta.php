<?php

namespace HRC;
use Illuminate\Database\Eloquent\Model;

class Etiqueta extends Model
{
   	protected $table = 'etiquetas';
    protected $connection = 'oficialia';
    protected $fillable = ['id','cve_oficialia','anio_libro','libros', 'lomo'];
    protected $hidden = [];
    public $timestamps = false;
}
