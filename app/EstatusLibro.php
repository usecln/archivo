<?php

namespace HRC;
use Illuminate\Database\Eloquent\Model;

class EstatusLibro extends Model
{
	protected $table = 'cat_estatus_libro';
	protected $primaryKey = 'cve_estatus_libro';
    protected $fillable = ['cve_estatus_libro', 'nom_estatus_libro']; 
    protected $hidden = [];
}
