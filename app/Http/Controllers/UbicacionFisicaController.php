<?php

namespace HRC\Http\Controllers;
use Illuminate\Http\Request;
use HRC\UbicacionFisica;

class UbicacionFisicaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        return UbicacionFisica::all();
    }

    public function show($id)
    {
        return UbicacionFisica::find($id);
    }
}
