<?php

namespace HRC\Http\Controllers;
use Illuminate\Http\Request;
use HRC\EstatusLibro;

class EstatusController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        return EstatusLibro::all();
    }

    public function show($id)
    {
        return EstatusLibro::find($id);
    }
}
