<?php

namespace HRC\Http\Controllers;
use Illuminate\Http\Request;
use HRC\TipoFormato;

class TipoFormatoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        return TipoFormato::all();
    }

    public function show($id)
    {
        return TipoFormato::find($id);
    }
}
