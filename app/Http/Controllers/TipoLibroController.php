<?php

namespace HRC\Http\Controllers;
use Illuminate\Http\Request;
use HRC\TipoLibro;

class TipoLibroController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        return TipoLibro::all();
    }

    public function show($id)
    {
        return TipoLibro::find($id);
    }
}