<?php

namespace HRC\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use HRC\MovLibro;
use HRC\Empleado;

class MovLibroController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        // return MovLibro::all();
    }

    public function show(Request $request, $id)
    {
    	switch ($id) {
    		case 1:
    			// select m.cve_empleado, e.nombre, e.primer_ap, e.segundo_ap, g.nom_grupo, Numero = count(*) 
				// from mov_libros m inner join empleados e
				// on m.cve_empleado = e.cve_empleado inner join cat_grupos g on e.cve_grupo = g.cve_grupo  
				// where m.cve_movto_libro in(4,5,6,40,41,42,43) group by m.cve_empleado, e.nombre,e.primer_ap, e.segundo_ap, g.nom_grupo
    			// se usa en el modulo "control de libros solicitados"
    			
    			return DB::table('mov_libros')
					->select('mov_libros.cve_empleado', 'empleados.nombre', 'empleados.primer_ap', 'empleados.segundo_ap', 'cat_grupos.nom_grupo', 
						DB::raw('count(*) as numero'))
					->join('empleados', 'mov_libros.cve_empleado', '=', 'empleados.cve_empleado')
					->join('cat_grupos', 'empleados.cve_grupo', '=', 'cat_grupos.cve_grupo')
					->whereIn('mov_libros.cve_movto_libro', [4,5,6,40,41,42,43])
					->groupBy('mov_libros.cve_empleado')
					->groupBy('empleados.nombre')
					->groupBy('empleados.primer_ap')
					->groupBy('empleados.segundo_ap')
					->groupBy('cat_grupos.nom_grupo')
				    ->get();
    		break;
            case 2:
                // Select * from mov_libros m(nolock) where m.cve_empleado = 26 
                // and cve_movto_libro in(4,5,6,40,41,42,43) and m.observacion is null
                $this->validate($request, [
                    'cve_empleado' => 'required|numeric'
                ]);
                return DB::table('mov_libros')
                    ->select(DB::raw('count(*) as count'))
                    ->where('cve_empleado', $request->cve_empleado)
                    ->whereNull('observacion')
                    ->whereIn('mov_libros.cve_movto_libro', [4,5,6,40,41,42,43])
                    ->get()[0]->count;
            break;
            default:
            break;
    	}
    }
}