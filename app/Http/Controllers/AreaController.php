<?php

namespace HRC\Http\Controllers;
use HRC\Area;

class AreaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        return Area::all();
    }

    public function show($id)
    {
        return Area::find($id);
    }
}
