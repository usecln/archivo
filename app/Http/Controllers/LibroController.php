<?php

namespace HRC\Http\Controllers;
use Illuminate\Http\Request;
use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;
use Illuminate\Support\Facades\DB;
use HRC\Libro;
use HRC\MovActa;
use HRC\Logger;
use JWTAuth;
use PDF;
class LibroController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function show(Request $request, $id)
    {
        $user = JWTAuth::parseToken()->toUser();
    	switch ($id) {
    		case 1:
    			// id = 1	->  where cve_oficialia like x, where anio_libro = y where num_lomo = z
    			//				with ?
    			$this->validate($request, [
		            'cve_oficialia' => 'required',
		            'anio_libro' => 'required|numeric|min:1850',
		            'num_lomo' => 'required|numeric',
		        ]);
		        return Libro::on($request->has('sic_oficialia')? 'oficialia': 'archivo')
                            ->where('cve_oficialia', 'like', $request->cve_oficialia)
		        			->where('anio_libro', $request->anio_libro)
		        			->where('num_lomo', $request->num_lomo)
		        			->get();
    			break;
    		case 2:
    			// id = 1	->  where cve_oficialia like x, where cve_tipo_libro = y where anio_libro = z
    			//				with('condicion','ubicacion_fisica','tipo_formato','tipo_formato','estatus_libro')
    			//				se usa para el modulo inventario de libros link '/inventario'
    			$this->validate($request, [
		            'cve_oficialia' => 'required',
		            'anio_libro' => 'required|numeric|min:1850',
		            'tipo_libro' => 'required|numeric',
		        ]);

                //'id','user_id','ip','cve_oficialia','anio_libro','num_lomo','cve_tipo_libro','usuario','hrc','accion','fecha' 
                if($user->id <> 1) // no registrar acciones de usuario admin
                    Logger::create([
                        'user_id' => $user->id, 
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'cve_oficialia' => $request->cve_oficialia,
                        'anio_libro' => $request->anio_libro,
                        'cve_tipo_libro' => $request->tipo_libro,
                        'hrc' => $request->has('sic_oficialia')? 'oficialia': 'archivo',
                        'usuario' => $user->name,
                        'accion' => 'Consultó inventario de libros',
                        'fecha' => \Carbon\Carbon::now()
                    ]);

                if(!$request->has('order'))
    		        return Libro::on($request->has('sic_oficialia')? 'oficialia': 'archivo')
                                ->with('condicion','ubicacion_fisica','tipo_formato','estatus_libro')
    		        			->where('cve_oficialia', 'like', $request->cve_oficialia)
    		        			->where('anio_libro', $request->anio_libro)
    		        			->where('cve_tipo_libro', $request->tipo_libro)
    		        			->get();

                if($request->order{0} == "-")
                {
                    $order = substr($request->order, 1, strlen($request->order)-1);
                    $ordermod = "desc";
                }
                else
                {
                    $order = $request->order;
                    $ordermod = "asc";
                }

                if (in_array($order, array("cve_condicion", "cve_ubicacion", "cve_tipo_formato", "cve_estatus_libro"))) {
                    // ordenar basado en el id del hijo no por el texto, falta implementar el ordenamiento
                    return Libro::on($request->has('sic_oficialia')? 'oficialia': 'archivo')
                                ->with('condicion','ubicacion_fisica','tipo_formato','estatus_libro')
                                ->where('cve_oficialia', 'like', $request->cve_oficialia)
                                ->where('anio_libro', $request->anio_libro)
                                ->where('cve_tipo_libro', $request->tipo_libro)
                                ->orderBy($order, $ordermod)
                                ->get();
                }
                else { // ordenar directamente por un atributo
                    return Libro::on($request->has('sic_oficialia')? 'oficialia': 'archivo')
                                ->with('condicion','ubicacion_fisica','tipo_formato','estatus_libro')
                                ->where('cve_oficialia', 'like', $request->cve_oficialia)
                                ->where('anio_libro', $request->anio_libro)
                                ->where('cve_tipo_libro', $request->tipo_libro)
                                ->orderBy($order, $ordermod)
                                ->get();
                }

    			break;
            case 3:
                // id = 3   ->  Select anio_libro, count(*) as cuantos from libros where cve_oficialia='01003' and cve_tipo_libro=1 group by anio_libro
                // se usa se usa en el dialog detalles de años capturados del modulo inventario de libros link '/inventario'
                $this->validate($request, [
                    'cve_oficialia' => 'required',
                    'cve_tipo_libro' => 'required|numeric',
                ]);

                //'id','user_id','ip','cve_oficialia','anio_libro','num_lomo','cve_tipo_libro','usuario','hrc','accion','fecha' 
                if($user->id <> 1) // no registrar acciones de usuario admin
                    Logger::create([
                        'user_id' => $user->id, 
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'cve_oficialia' => $request->cve_oficialia,
                        'cve_tipo_libro' => $request->tipo_libro,
                        'hrc' => $request->has('sic_oficialia')? 'oficialia': 'archivo',
                        'usuario' => $user->name,
                        'accion' => 'Consultó años con libros en inventario de libros',
                        'fecha' => \Carbon\Carbon::now()
                    ]);

                return DB::connection($request->has('sic_oficialia')? 'oficialia': 'archivo')
                     ->table('libros')->select(DB::raw('anio_libro, count(*) as libros'))
                     ->where('cve_oficialia', 'like', $request->cve_oficialia)
                     ->where('cve_tipo_libro', $request->cve_tipo_libro)
                     ->whereNull('deleted_at')
                     ->groupBy('anio_libro')
                     ->orderby('anio_libro', 'asc')
                     ->get();

                break;
            case 4:
                // id = 4   ->  Select anio_libro, count(*) as cuantos from libros where cve_oficialia='01003' and cve_tipo_libro=1 group by anio_libro
                // se usa se usa en el dialog detalles de años capturados del modulo inventario de libros link '/inventario'
                $this->validate($request, [
                    'cve_oficialia' => 'required',
                    'anio_libro' => 'required|numeric|min:1900',
                    'num_lomo' => 'required|numeric',
                ]);

                //'id','user_id','ip','cve_oficialia','anio_libro','num_lomo','cve_tipo_libro','usuario','hrc','accion','fecha' 
                if($user->id <> 1) // no registrar acciones de usuario admin
                    Logger::create([
                        'user_id' => $user->id, 
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'cve_oficialia' => $request->cve_oficialia,
                        'anio_libro' => $request->anio_libro,
                        'num_lomo' => $request->num_lomo,
                        'hrc' => $request->has('sic_oficialia')? 'oficialia': 'archivo',
                        'usuario' => $user->name,
                        'accion' => 'Consultó detalles de libro en inventario de libros',
                        'fecha' => \Carbon\Carbon::now()
                    ]);

                return Libro::on($request->has('sic_oficialia')? 'oficialia': 'archivo')
                        ->with('oficialia', 'tipo_libro', 'tipo_formato', 'ubicacion_fisica', 'condicion', 'estatus_libro')
                        ->where('cve_oficialia', 'like', $request->cve_oficialia)
                        ->where('anio_libro', $request->anio_libro)
                        ->where('num_lomo', $request->num_lomo)
                        ->get();

                break;
            case 5:
                // id = 5   ->  Select distinct m.cve_oficialia,m.anio_libro,m.num_lomo,m.id,n.acta 
                //              from mov_actas m join nacimientos n
                //              on m.cve_oficialia = n.cve_oficialia
                //              and m.anio_libro=n.anio_libro
                //              and m.num_lomo=n.num_lomo
                //              and m.id=n.id
                //              where m.cve_oficialia = '06001' and m.anio_libro = 1995 and m.num_lomo = 19701 and m.cve_movto_acta = 1;
                //              cve_movto_acta es fijo siempre 1
                // se usa se usa en el dialog actas trabajadas del modulo inventario de libros link '/inventario'
                $this->validate($request, [
                    'cve_oficialia' => 'required',
                    'anio_libro' => 'required|numeric|min:1850',
                    'num_lomo' => 'required|numeric',
                ]);

                //'id','user_id','ip','cve_oficialia','anio_libro','num_lomo','cve_tipo_libro','usuario','hrc','accion','fecha' 
                if($user->id <> 1) // no registrar acciones de usuario admin
                    Logger::create([
                        'user_id' => $user->id, 
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'cve_oficialia' => $request->cve_oficialia,
                        'anio_libro' => $request->anio_libro,
                        'num_lomo' => $request->num_lomo,
                        'hrc' => $request->has('sic_oficialia')? 'oficialia': 'archivo',
                        'usuario' => $user->name,
                        'accion' => 'Consultó actas trabajadas en inventario de libros',
                        'fecha' => \Carbon\Carbon::now()
                    ]);

                return MovActa::on($request->has('sic_oficialia')? 'oficialia': 'archivo')
                        ->with(array('nacimiento' => function($q) use ($request) {
                            $q->where('cve_oficialia', $request->cve_oficialia);
                            $q->where('anio_libro', $request->anio_libro);
                            $q->where('num_lomo', $request->num_lomo);
                        }))
                        ->where('cve_oficialia', $request->cve_oficialia)
                        ->where('anio_libro', $request->anio_libro)
                        ->where('num_lomo', $request->num_lomo)
                        ->where('cve_movto_acta', 1)
                        ->get();
                break;
            case 6:
                // libros no enviados a digital
                // se usa en el modulo "Enviar a digital"
                return Libro::on($request->has('sic_oficialia')? 'oficialia': 'archivo')
                        ->with('oficialia', 'tipo_libro', 'tipo_formato', 'ubicacion_fisica', 'condicion', 'estatus_libro')
                        ->where('anio_libro', '>=', 1930)
                        ->where('cve_ubicacion', 1)
                        ->where('cve_estatus_libro', 3)
                        ->whereYear('fecha_comprobante', '=', substr($request->fecha_comprobante, 0, 4))
                        ->whereMonth('fecha_comprobante', '=', substr($request->fecha_comprobante, 5, 2))
                        ->whereDay('fecha_comprobante', '=', substr($request->fecha_comprobante, 8, 2))
                        ->orderBy('cve_oficialia')
                        ->orderBy('anio_libro')
                        ->orderBy('cve_tipo_libro')
                        ->get();
            break;
            case 7:
                // libros no enviados a archivo (desde visitador)
                // se usa en el modulo "Enviar a archivo"
                return Libro::on($request->has('sic_oficialia')? 'oficialia': 'archivo')
                        ->with('oficialia', 'tipo_libro', 'tipo_formato', 'ubicacion_fisica', 'condicion', 'estatus_libro')
                        ->where('anio_libro', '>=', 1930)
                        ->where('cve_ubicacion', 1)
                        ->where('cve_estatus_libro', 25)
                        ->orderBy('cve_oficialia')
                        ->orderBy('anio_libro')
                        ->orderBy('cve_tipo_libro')
                        ->get();
            break;
            case 8:
                // se usa en el modulo asignacion de carga de trabajo
                // recive tipo de libro y si recive oficialia regresa libos pendientes de esa oficialia
                // si no recive oficialia regresa todas las oficialias
                $this->validate($request, ['cve_tipo_libro' => 'required|numeric']);

                if($user->id <> 1) // no registrar acciones de usuario admin
                    Logger::create([
                        'user_id' => $user->id, 
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'cve_oficialia' => $request->has('cve_oficialia')? $request->cve_oficialia: null,
                        'cve_tipo_libro' => $request->cve_tipo_libro,
                        'hrc' => $request->has('sic_oficialia')? 'oficialia': 'archivo',
                        'usuario' => $user->name,
                        'accion' => 'Consultó libros pendientes de liberar en digital',
                        'fecha' => \Carbon\Carbon::now()
                    ]);

                $order = 'cve_oficialia';
                $ordermod = 'asc';
                if($request->has('order')) {
                    if($request->order{0} == "-")
                    {
                        $order = substr($request->order, 1, strlen($request->order)-1);
                        $ordermod = "desc";
                    }
                    else
                    {
                        $order = $request->order;
                        $ordermod = "asc";
                    }
                }
                if($request->has('cve_oficialia')) // buscar para una oficialia
                {
                    return Libro::on($request->has('sic_oficialia')? 'oficialia': 'archivo')
                                ->where('cve_tipo_libro', $request->cve_tipo_libro)
                                ->where('cve_oficialia', $request->cve_oficialia)
                                ->where('cve_estatus_libro', 2)
                                ->where('cve_ubicacion', 9)
                                ->orderBy($order, $ordermod)
                                ->get();
                }
                else // para todas las oficialias
                {
                    return Libro::on($request->has('sic_oficialia')? 'oficialia': 'archivo')
                                ->where('cve_tipo_libro', $request->cve_tipo_libro)
                                ->where('cve_estatus_libro', 2)
                                ->where('cve_ubicacion', 9)
                                ->orderBy($order, $ordermod)
                                ->get();
                }
            break;
            case 9:
                // libros enviados a archivo (desde visitador)
                // se usa en el modulo "Enviar a archivo"
                return Libro::on($request->has('sic_oficialia')? 'oficialia': 'archivo')
                        ->with('oficialia', 'tipo_libro', 'tipo_formato', 'ubicacion_fisica', 'condicion', 'estatus_libro')
                        ->where('anio_libro', '>=', 1930)
                        ->where('cve_ubicacion', 1)
                        ->where('cve_estatus_libro', 3)
                        ->whereYear('updated_at', '=', substr($request->fecha_envio, 0, 4))
                        ->whereMonth('updated_at', '=', substr($request->fecha_envio, 5, 2))
                        ->whereDay('updated_at', '=', substr($request->fecha_envio, 8, 2))
                        ->orderBy('cve_oficialia')
                        ->orderBy('anio_libro')
                        ->orderBy('cve_tipo_libro')
                        ->get();
            break;
            case 10:
                // libros enviados a digital (desde archivo)
                // se usa en el modulo "Enviar a digital"
                return Libro::on($request->has('sic_oficialia')? 'oficialia': 'archivo')
                        ->with('oficialia', 'tipo_libro', 'tipo_formato', 'ubicacion_fisica', 'condicion', 'estatus_libro')
                        ->where('anio_libro', '>=', 1930)
                        ->where('cve_ubicacion', 9)
                        ->where('cve_estatus_libro', 2)
                        ->whereYear('updated_at', '=', substr($request->fecha_envio, 0, 4))
                        ->whereMonth('updated_at', '=', substr($request->fecha_envio, 5, 2))
                        ->whereDay('updated_at', '=', substr($request->fecha_envio, 8, 2))
                        ->orderBy('cve_oficialia')
                        ->orderBy('anio_libro')
                        ->orderBy('cve_tipo_libro')
                        ->get();
            break;
            case 11:
                $this->validate($request, [
                    'cve_oficialia' => 'required',
                    'anio_libro' => 'required|numeric|min:1850',
                    'num_lomo' => 'required|numeric',
                ]);
                $fechas = Logger::where('accion', 'NOT LIKE', 'Consultó%' )
                        ->where('cve_oficialia', 'like', $request->cve_oficialia)
                        ->where('anio_libro', $request->anio_libro)
                        ->where('num_lomo', $request->num_lomo)
                        ->orderby('fecha','asc')
                        ->get();
                if($user->id <> 1) // no registrar acciones de usuario admin
                    Logger::create([
                        'user_id' => $user->id, 
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'cve_oficialia' => $request->cve_oficialia,
                        'anio_libro' => $request->anio_libro,
                        'num_lomo' => $request->num_lomo,
                        'hrc' => $request->has('sic_oficialia')? 'oficialia': 'archivo',
                        'usuario' => $user->name,
                        'accion' => 'Consultó histrorial de libro',
                        'fecha' => \Carbon\Carbon::now()
                    ]);
                return $fechas;
            break;
            case 12:
                // libros no enviados a digital agregados en archivo
                // se usa en el modulo "Enviar a digital"
                return Libro::on($request->has('sic_oficialia')? 'oficialia': 'archivo')
                        ->with('oficialia', 'tipo_libro', 'tipo_formato', 'ubicacion_fisica', 'condicion', 'estatus_libro')
                        ->where('anio_libro', '>=', 1930)
                        ->where('cve_ubicacion', 1)
                        ->where('cve_estatus_libro', 3)
                        ->where('created_at', '>=', $request->fecha_inicial.' 00:00:00.000')
                        ->where('created_at', '<=', $request->fecha_final.' 23:59:59.000')
                        ->orderBy('cve_oficialia')
                        ->orderBy('anio_libro')
                        ->orderBy('cve_tipo_libro')
                        ->get();
            break;
            case 13:
                return Libro::on($request->has('sic_oficialia')? 'oficialia': 'archivo')
                        ->with('oficialia', 'tipo_libro')
                        ->where('anio_libro', '>=', 1930)
                        ->where('cve_estatus_libro', '=', $request->area == 1? 3 : ($request->area == 2? 2 : ($request->area == 3? 3 : null)))
                        ->where('cve_ubicacion', $request->area == 1? 1 : ($request->area == 2? 9 : ($request->area == 3? 25 : null)))
                        ->orderBy('cve_oficialia')
                        ->orderBy('anio_libro')
                        ->orderBy('cve_tipo_libro')
                        ->get();
            break;
    		default:
    			return [];
    			break;
    	}
    }
    public function store(Request $request)
    {
        // si tiene data se usa para crear un nuevo libro, si ya existe lo actualiza
        $user = JWTAuth::parseToken()->toUser();
        if($request->has('data')) {
            $obj = (Object) $request->data;
            if(isset($obj->origin_ref)) {
                $prev = (Object) $obj->origin_ref; // datos anteriores para encontrar el libro en la bd
                Libro::on($request->has('sic_oficialia')? 'oficialia': 'archivo')
                    ->where('cve_oficialia', 'like', $prev->cve_oficialia)
                    ->where('anio_libro', $prev->anio_libro)
                    ->where('num_lomo', $prev->num_lomo)
                    ->update([
                        'cve_oficialia' => $obj->cve_oficialia,
                        'anio_libro' => $obj->anio_libro,
                        'num_lomo' => $obj->num_lomo,
                        'cve_condicion' => $obj->cve_condicion,
                        'cve_ubicacion' => $obj->cve_ubicacion,
                        'cve_tipo_formato' => $obj->cve_tipo_formato,
                        'cve_tipo_libro' => $obj->cve_tipo_libro,
                        'cve_estatus_libro' => $obj->cve_estatus_libro,
                        'num_libro' => $obj->num_libro,
                        'num_tomo' => $obj->num_tomo,
                        'num_actas' => $obj->num_actas,
                        'num_carton' => isset($obj->num_carton) ? $obj->num_carton : null,
                        'acta_inicial' => $obj->acta_inicial,
                        'acta_final' => $obj->acta_final,
                        'lomo' => $obj->lomo,
                        'flag' => $obj->flag,
                        'cve_ubicacion1' => $obj->cve_ubicacion1,
                        'rango' => $obj->rango,
                        'ok' => $obj->ok,
                        'deleted_at' => $obj->deleted_at,
                        'updated_at' => \Carbon\Carbon::now()
                        ]);
                if($user->id <> 1) // no registrar acciones de usuario admin
                    Logger::create([
                        'user_id' => $user->id, 
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'cve_oficialia' => $obj->cve_oficialia,
                        'anio_libro' => $obj->anio_libro,
                        'num_lomo' => $obj->num_lomo,
                        'hrc' => $request->has('sic_oficialia')? 'oficialia': 'archivo',
                        'usuario' => $user->name,
                        'accion' => 'Modificó libro'.(isset($prev) && $prev->num_lomo <> $obj->num_lomo? ', num_lomo anterior = '.$prev->num_lomo: ''),
                        'fecha' => \Carbon\Carbon::now()
                    ]);
                return 1; // 1 actualizado
            }
            else
            {
                $num_lomo = $obj->cve_tipo_libro.(strlen($obj->num_libro) < 2? '0'.$obj->num_libro: $obj->num_libro).(strlen($obj->num_tomo) < 2? '0'.$obj->num_tomo: $obj->num_tomo);
                if(isset($obj->cve_oficialia) && isset($obj->anio_libro) && 
                    count(Libro::on($request->has('sic_oficialia')? 'oficialia': 'archivo')
                        ->where('cve_oficialia', 'like', $obj->cve_oficialia)
                        ->where('anio_libro', $obj->anio_libro)
                        ->where('num_lomo', $num_lomo)
                        ->get()) > 0)
                    return 0; // ya existe                

                $lib = new Libro;
                $lib->changeConnection($request->has('sic_oficialia')? 'oficialia': 'archivo');
                $lib->cve_oficialia = $obj->cve_oficialia;
                $lib->num_lomo = $num_lomo;
                $lib->anio_libro = $obj->anio_libro;
                $lib->cve_condicion = $obj->cve_condicion;
                $lib->cve_ubicacion = $obj->cve_ubicacion;
                $lib->cve_tipo_formato = $obj->cve_tipo_formato;
                $lib->cve_tipo_libro = $obj->cve_tipo_libro;
                $lib->cve_estatus_libro = $obj->cve_estatus_libro;
                $lib->num_libro = $obj->num_libro;
                $lib->num_tomo = isset($obj->num_tomo)? $obj->num_tomo: 1;
                $lib->num_actas = $obj->num_actas;
                if(!$request->has('sic_oficialia')){
                    $lib->num_carton = isset($obj->num_carton) ? $obj->num_carton : null;
                    $lib->ok = isset($obj->ok)? $obj->ok: null;
                }
                $lib->acta_inicial = $obj->acta_inicial;
                $lib->acta_final = $obj->acta_final;
                $lib->lomo = isset($obj->lomo)? $obj->lomo: 9;
                $lib->flag = isset($obj->flag)? $obj->flag: null;
                $lib->cve_ubicacion1 = isset($obj->cve_ubicacion1)? $obj->cve_ubicacion1: null;
                $lib->rango = isset($obj->rango)? $obj->rango: null;
                $lib->created_at = isset($obj->created_at)? $obj->created_at: \Carbon\Carbon::now();
                $lib->updated_at = \Carbon\Carbon::now();
                $lib->deleted_at = isset($obj->deleted_at)? $obj->deleted_at: null;
                $lib->save();

                //'id','user_id','ip','cve_oficialia','anio_libro','num_lomo','cve_tipo_libro','usuario','hrc','accion','fecha' 
                if($user->id <> 1) // no registrar acciones de usuario admin
                {
                    Logger::create([
                        'user_id' => $user->id, 
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'cve_oficialia' => $lib->cve_oficialia,
                        'anio_libro' => $lib->anio_libro,
                        'num_lomo' => $lib->num_lomo,
                        'hrc' => $request->has('sic_oficialia')? 'oficialia': 'archivo',
                        'usuario' => $user->name,
                        'accion' => 'Agregó libro',
                        'fecha' => \Carbon\Carbon::now()
                    ]);

                    if($lib->cve_ubicacion == 9 && $lib->cve_estatus_libro == 2) {
                        Logger::create([
                            'user_id' => $user->id, 
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'cve_oficialia' => $lib->cve_oficialia,
                            'anio_libro' => $lib->anio_libro,
                            'num_lomo' => $lib->num_lomo,
                            'hrc' => $request->has('sic_oficialia')? 'oficialia': 'archivo',
                            'usuario' => $user->name,
                            'accion' => 'Envió libro a digital',
                            'fecha' => \Carbon\Carbon::now()
                        ]);
                    }
                }
                
                return 1; // guardado
            }
        }
        else if($request->has('enviar_digital')) {
                // se usa en el modulo enviar-digital para enviar libros a digital
                foreach($request->enviar_digital as $enviar) {
                    $lib = (object) $enviar;
                    Libro::where('cve_oficialia', 'like', $lib->cve_oficialia)
                        ->where('anio_libro', $lib->anio_libro)
                        ->where('num_lomo', $lib->num_lomo)
                        ->update([
                            'cve_ubicacion' => 9,
                            'cve_estatus_libro' => 2,
                            'updated_at' => \Carbon\Carbon::now()
                        ]);
                    Logger::create([
                        'user_id' => $user->id, 
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'cve_oficialia' => $lib->cve_oficialia,
                        'anio_libro' => $lib->anio_libro,
                        'num_lomo' => $lib->num_lomo,
                        'hrc' => $request->has('sic_oficialia')? 'oficialia': 'archivo',
                        'usuario' => $user->name,
                        'accion' => 'Envió libro a digital',
                        'fecha' => \Carbon\Carbon::now()
                    ]);
                }
        }
        else if($request->has('enviar_archivo')) {
            // se usa en el modulo enviar-archivo para enviar libros a archivo
            foreach($request->enviar_archivo as $enviar) {
                $lib = (object) $enviar;
                Libro::where('cve_oficialia', 'like', $lib->cve_oficialia)
                    ->where('anio_libro', $lib->anio_libro)
                    ->where('num_lomo', $lib->num_lomo)
                    ->update([
                        'cve_estatus_libro' => 3,
                        'updated_at' => \Carbon\Carbon::now()
                    ]);
                Logger::create([
                    'user_id' => $user->id, 
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'cve_oficialia' => $lib->cve_oficialia,
                    'anio_libro' => $lib->anio_libro,
                    'num_lomo' => $lib->num_lomo,
                    'hrc' => $request->has('sic_oficialia')? 'oficialia': 'archivo',
                    'usuario' => $user->name,
                    'accion' => 'Envió libro a archivo',
                    'fecha' => \Carbon\Carbon::now()
                ]);
            }
        }
        else if($request->has('cambiar_ubicacion')) {
            // se usa en el modulo enviar-digital para enviar libros a otra ubicacion y no a digital
            $data = (object) $request->cambiar_ubicacion;
            foreach($data->libros as $libro) {
                $lib = (object) $libro;
                Libro::where('cve_oficialia', 'like', $lib->cve_oficialia)
                    ->where('anio_libro', $lib->anio_libro)
                    ->where('num_lomo', $lib->num_lomo)
                    ->update([
                        'cve_ubicacion' => $data->ubicacion,
                        'updated_at' => \Carbon\Carbon::now()
                    ]);
                Logger::create([
                    'user_id' => $user->id, 
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'cve_oficialia' => $lib->cve_oficialia,
                    'anio_libro' => $lib->anio_libro,
                    'num_lomo' => $lib->num_lomo,
                    'hrc' => $request->has('sic_oficialia')? 'oficialia': 'archivo',
                    'usuario' => $user->name,
                    'accion' => 'Cambió a ubicación: '.$data->ubicacion,
                    'fecha' => \Carbon\Carbon::now()
                ]);
            }
        }
        else if($request->has('liberar')) {
            $libro = (object) $request->liberar;
            Libro::where('cve_oficialia', $libro->cve_oficialia)
                ->where('anio_libro', $libro->anio_libro)
                ->where('num_lomo', $libro->num_lomo)
                ->update([
                    'cve_ubicacion' => 23,
                    'cve_estatus_libro' => 24,
                    'updated_at' => \Carbon\Carbon::now()
                ]);
            if($user->id <> 1) // no registrar acciones de usuario admin
                Logger::create([
                    'user_id' => $user->id, 
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'cve_oficialia' => $libro->cve_oficialia,
                    'anio_libro' => $libro->anio_libro,
                    'num_lomo' => $libro->num_lomo,
                    'hrc' => $request->has('sic_oficialia')? 'oficialia': 'archivo',
                    'usuario' => $user->name,
                    'accion' => 'Liberó de digital',
                    'fecha' => \Carbon\Carbon::now()
                ]);
        }
        else if($request->has('comprobante')) {
            $req = (Object) $request->comprobante;
            $archivo = (isset($req->archivo))? true: false;
            $now = new \DateTime('now');
            $day = $now->format('d');
            $mn = $now->format('m');
            $year = $now->format('Y');
            switch (intval($mn)) {
                case 1:
                    $month = "Enero";
                    break;
                case 2:
                    $month = "Febrero";
                    break;
                case 3:
                    $month = "Marzo";
                    break;
                case 4:
                    $month = "Abril";
                    break;
                case 5:
                    $month = "Mayo";
                    break;
                case 6:
                    $month = "Junio";
                    break;
                case 7:
                    $month = "Julio";
                    break;
                case 8:
                    $month = "Agosto";
                    break;
                case 9:
                    $month = "Septiembre";
                    break;
                case 10:
                    $month = "Octubre";
                    break;
                case 11:
                    $month = "Nobiembre";
                    break;
                case 12:
                    $month = "Diciembre";
                    break;
            }
            foreach($req->libros as $hoja) {
                foreach($hoja as $libro) {
                    $lib = (object) $libro;
                    if($archivo)
                    {
                        Libro::where('cve_oficialia', 'like', $lib->cve_oficialia)
                            ->where('anio_libro', $lib->anio_libro)
                            ->where('num_lomo', $lib->num_lomo)
                            ->update([
                                'updated_at' => \Carbon\Carbon::now()
                            ]);
                    }
                    else
                    {
                        Libro::where('cve_oficialia', 'like', $lib->cve_oficialia)
                            ->where('anio_libro', $lib->anio_libro)
                            ->where('num_lomo', $lib->num_lomo)
                            ->update([
                                'fecha_comprobante' => \Carbon\Carbon::now()
                            ]);
                    }
                }
            }
            $data = [
                'id' => null,
                'libros' => $req->libros,
                'municipio' => 'Culiacán',
                'estado' => 'Sinaloa',
                'day' => $day,
                'month' => $month,
                'year' => $year,
                'nota' => '',
                'entrega' => $user->name,
                'area_entrega' => $archivo? 'archivo' : 'visitador',
                'recive' => $archivo? 'Víctor Chávez Landeros': 'Jesus Omar Burgueño Contreras',
                'area_recive' => $archivo? 'Digital' : 'Archivo'
            ];

            if (!file_exists('pdfs/comprobante/'))
                mkdir('pdfs/comprobante/', 0777, true);

            if (file_exists('pdfs/comprobante/'.$day.$mn.$year.'.pdf')) 
                unlink('pdfs/comprobante/'.$day.$mn.$year.'.pdf');

            $pdf = PDF::loadView('comprobante', $data);
            $pdf->save('pdfs/comprobante/comprobante'.$day.$mn.$year.'.pdf');
            
            $file = public_path().'/pdfs/comprobante/comprobante'.$day.$mn.$year.'.pdf';
            $pdfdata = file_get_contents($file);
            $base64 = base64_encode($pdfdata);
            
            return $base64;
        }
        else if($request->has('validate_exists')){
            $this->validate($request, [
                'cve_oficialia' => 'required',
                'anio_libro' => 'required|numeric|min:1850',
                'num_tomo' => 'required|numeric',
                'cve_tipo_libro' => 'required',
                'num_libro' => 'required'
            ]);
            $cve_tipos_libros = explode(',', $request->cve_tipo_libro);
            $num_libros = explode(',', $request->num_libro);

            array_pop($num_libros);
            if(sizeof($num_libros) <> sizeof($cve_tipos_libros))
                return 'error, sizeof($num_libros) <> sizeof($cve_tipos_libros)';

            return \DB::table("libros")
                      ->select("lomo as lomo", "cve_tipo_libro as cve_tipo_libro",
                            \DB::raw("(select max(lomo) + 1 from libros) as next_lomo")
                        )
                      ->where('cve_oficialia', $request->cve_oficialia)
                      ->where('anio_libro', $request->anio_libro)
                      ->where('num_tomo', $request->num_tomo)
                      ->where(function($query) use ($cve_tipos_libros, $num_libros) {
                            for($i = 0; $i < count($num_libros); $i++) {
                                $query->orwhere(function($q2) use ($cve_tipos_libros, $num_libros, $i) {
                                    $q2->where('cve_tipo_libro', $cve_tipos_libros{$i});
                                    $q2->where('num_libro', $num_libros{$i});
                                });
                            }
                      })
                      ->get();
        }
        else if($request->has('actualiza_lomos')){
                $this->validate($request, [
                    'cve_oficialia' => 'required',
                    'anio_libro' => 'required|numeric|min:1850',
                    'cve_tipo_libro' => 'required',
                    'num_libro' => 'required',
                    'num_lomo' => 'required'
                ]);

                $cve_tipos_libros = explode(',', $request->cve_tipo_libro);
                $num_libros = explode(',', $request->num_libro);

                array_pop($num_libros);
                if(sizeof($num_libros) <> sizeof($cve_tipos_libros))
                    return 'error, sizeof($num_libros) <> sizeof($cve_tipos_libros)';

                return \DB::table("libros")
                          ->where('cve_oficialia', $request->cve_oficialia)
                          ->where('anio_libro', $request->anio_libro)
                          ->where(function($query) use ($cve_tipos_libros, $num_libros) {
                                for($i = 0; $i < count($num_libros); $i++) {
                                    $query->orwhere(function($q2) use ($cve_tipos_libros, $num_libros, $i) {
                                        $q2->where('cve_tipo_libro', $cve_tipos_libros{$i});
                                        $q2->where('num_libro', $num_libros{$i});
                                    });
                                }
                          })
                          ->update(['lomo' => $request->num_lomo]);
        }
        else if($request->has('generate_barcode')){
            // si no existe imagen de codigo, generarla y guardarla!
            $this->validate($request, [
                'code' => 'required'
            ]);

            if (!file_exists('img/codigos_barras/'))
                mkdir('img/codigos_barras/', 0777, true);

            if (file_exists('img/codigos_barras/'.$request->code))
                return 'ya existia';

            // generar imagen
            $barcode = new BarcodeGenerator();
            $barcode->setText("$request->code");
            $barcode->setType(BarcodeGenerator::Code128);
            $barcode->setScale(1);
            $barcode->setThickness(40);
            $barcode->setFontSize(8);
            $code = $barcode->generate();
            // guardar imagen
            $data = base64_decode($code);
            file_put_contents('img/codigos_barras/'.$request->code, $data);
            return 'generada!';
        }
    }
    public function destroy(Request $request)
    {
        try { $user = JWTAuth::parseToken()->toUser(); } catch(Exception $e) { return ; }
        if(!isset($user->perfil) || (isset($user->perfil) && strlen($user->perfil) < 1) || (isset($user->perfil) && strlen($user->perfil) >= 1 && (intval($user->perfil{0}) <> 2 && intval($user->perfil{0}) <> 4 && intval($user->perfil{0}) <> 5)))
            return 'sin permiso';         
        $this->validate($request, [
                    'cve_oficialia' => 'required',
                    'anio_libro' => 'required|numeric|min:1850',
                    'num_lomo' => 'required|numeric',
                ]);

        //'id','user_id','ip','cve_oficialia','anio_libro','num_lomo','cve_tipo_libro','usuario','hrc','accion','fecha' 
        // if($user->id <> 1) // no registrar acciones de usuario admin
            Logger::create([
                'user_id' => $user->id, 
                'ip' => $_SERVER['REMOTE_ADDR'],
                'cve_oficialia' => $request->cve_oficialia,
                'anio_libro' => $request->anio_libro,
                'num_lomo' => $request->num_lomo,
                'hrc' => $request->has('sic_oficialia')? 'oficialia': 'archivo',
                'usuario' => $user->name,
                'accion' => 'Eliminó libro',
                'fecha' => \Carbon\Carbon::now()
            ]);

        Libro::on($request->has('sic_oficialia')? 'oficialia': 'archivo')
            ->where('cve_oficialia', $request->cve_oficialia)
            ->where('anio_libro', $request->anio_libro)
            ->where('num_lomo', $request->num_lomo)
            ->delete();
    }
}