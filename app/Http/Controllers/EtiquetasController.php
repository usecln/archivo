<?php

namespace HRC\Http\Controllers;
use Illuminate\Http\Request;
use PDF;
use HRC\Etiqueta;

class EtiquetasController extends Controller
{
	public function etiquetas(Request $request)
    {
    	$req = (Object) $request->data;
    	$now = new \DateTime('now');
        $day = $now->format('d');
        $mn = $now->format('m');
        $year = $now->format('Y');
        switch (intval($mn)) {
            case 1:
                $month = "Enero";
                break;
            case 2:
                $month = "Febrero";
                break;
            case 3:
                $month = "Marzo";
                break;
            case 4:
                $month = "Abril";
                break;
            case 5:
                $month = "Mayo";
                break;
            case 6:
                $month = "Junio";
                break;
            case 7:
                $month = "Julio";
                break;
            case 8:
                $month = "Agosto";
                break;
            case 9:
                $month = "Septiembre";
                break;
            case 10:
                $month = "Octubre";
                break;
            case 11:
                $month = "Nobiembre";
                break;
            case 12:
                $month = "Diciembre";
                break;
        }
        $data = [
            'etiquetas' => isset($req->etiquetas)? $req->etiquetas : []
        ];

        if (!file_exists('pdfs/etiquetas/'))
            mkdir('pdfs/etiquetas/', 0777, true);

        if (file_exists('pdfs/etiquetas/etiqueta'.$day.$mn.$year.'.pdf')) 
            unlink('pdfs/etiquetas/etiqueta'.$day.$mn.$year.'.pdf');

        $pdf = PDF::loadView('etiquetas', $data);
        $pdf->save('pdfs/etiquetas/etiqueta'.$day.$mn.$year.'.pdf');
        
        $file = public_path().'/pdfs/etiquetas/etiqueta'.$day.$mn.$year.'.pdf';
        $pdfdata = file_get_contents($file);
        $base64 = base64_encode($pdfdata);
        
        return $pdfdata;
    }
    public function guarda_etiqueta(Request $request)
    {
        $e = Etiqueta::where('lomo', $request->data['lomo'])->get();
        if(count($e) == 0) {
            Etiqueta::create([
                'cve_oficialia' => $request->data['cve_oficialia'],
                'anio_libro' => $request->data['anio_libro'],
                'libros' => $request->data['libros'],
                'lomo' => $request->data['lomo'],
            ]);
            $e = Etiqueta::where('lomo', $request->data['lomo'])->get();
            return ['id' => $e[0]->id];
        }
        else
        {
            $e[0]->cve_oficialia = $request->data['cve_oficialia'];
            $e[0]->anio_libro = $request->data['anio_libro'];
            $e[0]->libros = $request->data['libros'];
            $e[0]->save();
            return ['id' => $e[0]->id];
        }
    }
    public function buscar_etiqueta(Request $request)
    {
        $numeros = $request->get('numero');
        $numeros = \explode(',', $numeros);
        //$e = Etiqueta::where('lomo', $request->get('numero'))
         //   ->orWhere('id', $request->get('numero'))->get();
        $e = Etiqueta::whereIn('lomo', $numeros)
            ->orWhereIn('id', $numeros)->get();
        return $e;
    }
}