<?php

namespace HRC\Http\Controllers;
use Illuminate\Http\Request;
use HRC\Condicion;

class CondicionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        return Condicion::orderBy('nom_condicion', 'asc')->get();
    }

    public function show($id)
    {
        return Condicion::find($id);
    }
}
