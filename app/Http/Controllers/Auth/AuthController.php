<?php

namespace HRC\Http\Controllers\Auth;

use Auth;
use JWTAuth;
use HRC\User;
use HRC\Logger;
use Illuminate\Http\Request;
use HRC\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|min:4',
            'password' => 'required|min:4',
        ]);

        $credentials = $request->only('username', 'password');

        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->error('Datos incorrectos', 401);
            }
        } catch (\JWTException $e) {
            return response()->error('Error al crear token', 500);
        }

        // $user = Auth::user();
        $user = User::find(Auth::user()->id);
        if($_SERVER['REMOTE_ADDR'] <> '10.10.127.10' && $user->username <> 'admin') // (ip del server) todos pueden iniciar sesion desde el server, admin puede iniciar donde sea
        {
            if(isset($user->ip))
            {
                if($_SERVER['REMOTE_ADDR'] <> $user->ip)
                {
                    Logger::create([
                        'user_id' => $user->id, 
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'usuario' => $user->name,
                        'accion' => 'Intentó iniciar sesión en equipo no permitido',
                        'fecha' => \Carbon\Carbon::now()
                    ]);
                    return response()->error('Acceso restringido', 403);
                }
            }
        }
        
        Logger::create([
                'user_id' => $user->id, 
                'ip' => $_SERVER['REMOTE_ADDR'],
                'usuario' => $user->name,
                'accion' => 'Inició sesión',
                'fecha' => \Carbon\Carbon::now()
            ]);
        return response()->success(compact('user', 'token'));
    }

    public function register(Request $request)
    {
        $userx = JWTAuth::parseToken()->toUser();
        //'id','user_id','ip','cve_oficialia','anio_libro','num_lomo','cve_tipo_libro','usuario','hrc','accion','fecha' 
        if($userx->id <> 1) // no es admin
        {
            Logger::create([
                'user_id' => $userx->id, 
                'ip' => $_SERVER['REMOTE_ADDR'],
                'usuario' => $userx->name,
                'accion' => 'Intentó registrar usuario sin ser admin',
                'fecha' => \Carbon\Carbon::now()
            ]);
            return response()->error('Acceso restringido', 403);
        }
        $this->validate($request, [
            'name'       => 'required|min:3',
            'email'      => 'email|unique:users',
            'username'   => 'required|min:5|unique:users',
            'password'   => 'required|min:4',
            'ip'         => 'min:10',
            'perfil'     => 'required|integer'
        ]);

        $user = new User;
        $user->name = trim($request->name);
        $user->username = trim($request->username);
        $user->email = isset($request->email) && strlen(trim(strtolower($request->email))) > 0? trim(strtolower($request->email)) : null;
        $user->password = bcrypt($request->password);
        $user->ip = isset($request->ip) && strlen($request->ip) > 0 ? $request->ip : null;
        $user->perfil = isset($request->perfil) && strlen($request->perfil) > 0 ? $request->perfil : null;
        $user->save();

        $token = JWTAuth::fromUser($user);

        Logger::create([
                'user_id' => $userx->id, 
                'ip' => $_SERVER['REMOTE_ADDR'],
                'usuario' => $userx->name,
                'accion' => 'Registró nuevo usuario: '.$user->name,
                'fecha' => \Carbon\Carbon::now()
            ]);
        return response()->success(compact('user', 'token'));
    }
}