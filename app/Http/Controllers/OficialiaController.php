<?php

namespace HRC\Http\Controllers;
use Illuminate\Http\Request;
use HRC\Oficialia;

class OficialiaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        return Oficialia::with(['municipio' => function ($query) {
                        $query->where('cve_estado', 25);
                    }])->get();
    }

    public function show($id)
    {
        return Oficialia::with(['municipio' => function ($query) {
                        $query->where('cve_estado', 25);
                    }])->find($id);
    }
}