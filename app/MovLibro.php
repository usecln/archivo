<?php

namespace HRC;
use Illuminate\Database\Eloquent\Model;

class MovLibro extends Model
{
	protected $table = 'mov_libros';
	//protected $primaryKey = ''; // composite key (cve_oficialia, anio_libro, num_lomo, cve_movto_libro, fecha_mov_libro) unsupported
    protected $fillable = ['cve_oficialia', 'anio_libro', 'num_lomo', 'cve_movto_libro', 'fecha_mov_libro', 'cve_empleado', 'observacion']; 
    protected $hidden = [];
    public function empleado()
    {        
        return $this->hasOne('HRC\Empleado', 'cve_empleado', 'cve_empleado');
    }
}