<?php

namespace HRC;

use Illuminate\Database\Eloquent\Model;

class Condicion extends Model
{
	protected $table = 'cat_condicion_libro';
	protected $primaryKey = 'cve_condicion';
	protected $fillable = ['cve_condicion', 'nom_condicion']; 
	protected $hidden = [];
}
