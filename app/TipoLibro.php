<?php

namespace HRC;
use Illuminate\Database\Eloquent\Model;

class TipoLibro extends Model
{
	protected $table = 'cat_tipo_libros';
	protected $primaryKey = 'cve_tipo_libro';
    protected $fillable = ['cve_tipo_libro', 'nom_tipo_libro']; 
    protected $hidden = []; 
}