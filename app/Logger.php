<?php

namespace HRC;
use Illuminate\Database\Eloquent\Model;

class Logger extends Model
{
    protected $table = 'logger';
    protected $connection = 'archivo';
    protected $fillable = ['id','user_id','ip','cve_oficialia','anio_libro','num_lomo','cve_tipo_libro','usuario','hrc','accion','fecha']; 
    protected $hidden = [];
    protected $dates = ['fecha'];
    public $timestamps = false;
}