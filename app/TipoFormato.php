<?php

namespace HRC;
use Illuminate\Database\Eloquent\Model;

class TipoFormato extends Model
{
	protected $table = 'cat_tipo_formato';
	protected $primaryKey = 'cve_tipo_formato';
    protected $fillable = ['cve_tipo_formato', 'nom_tipo_formato']; 
    protected $hidden = [];
}
