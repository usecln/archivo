<?php

namespace HRC;

use Illuminate\Database\Eloquent\Model;

class UbicacionFisica extends Model
{
	protected $table = 'cat_ubicacion_fisica';
	protected $primaryKey = 'cve_ubicacion';
    protected $fillable = ['cve_ubicacion', 'nom_ubicacion']; 
    protected $hidden = [];
}
