<?php

namespace HRC;

use Illuminate\Database\Eloquent\Model;

class UbicacionTemporal extends Model
{
	protected $table = 'cat_ubicacion_temporal';
	protected $primaryKey = 'cve_ubicacion';
    protected $fillable = ['cve_ubicacion', 'nom_ubicacion']; 
    protected $hidden = [];
}
