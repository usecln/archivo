<?php

namespace HRC;

use Illuminate\Database\Eloquent\Model;

class Oficialia extends Model
{
	protected $table = 'cat_oficialias';
	protected $primaryKey = 'cve_oficialia';
    protected $fillable = ['cve_oficialia', 'nom_oficialia', 'fec_inicio']; 
    protected $hidden = [];

    public function getCveOficialiaAttribute($value)
    {
        return (string) $value;
    }
    public function municipio()
    {        
        return $this->hasOne('HRC\Municipio', 'cve_municipio', 'cve_municipio');
    }
}
