<?php

namespace HRC;

use Illuminate\Database\Eloquent\Model;

class Nacimiento extends Model
{
	// protected $table = 'nacimientos';
	// protected $primaryKey = 'id';
    protected $fillable = ['cve_oficialia','anio_libro','num_lomo','id','cve_comparece','cve_pais','cve_estado','cve_municipio','cve_tipo_anotacion','acta','foja','fecha_reg','nombre','primer_ap','segundo_ap','sexo','fecha_nac','localidad','reg_vivo','crip','curp','no_certif','liga_imagen','datos_aceptados','imagen_aceptada','nota_marginal','cve_empleado','id_gemelo']; 
    protected $hidden = [];

    public function getCveOficialiaAttribute($value)
    {
        return (string) $value;
    }    
}