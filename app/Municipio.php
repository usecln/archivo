<?php

namespace HRC;
use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
	protected $table = 'cat_municipios';
    protected $fillable = [
    	'cve_municipio', 'municipio'
    ];
}