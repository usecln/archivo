<?php

namespace HRC;
use Illuminate\Database\Eloquent\Model;

class MovActa extends Model
{
	protected $table = 'mov_actas';
	//protected $primaryKey = 'id';
    protected $fillable = ['cve_movto_acta','cve_oficialia','anio_libro','num_lomo','id','fecha_mov_acta','cve_empleado','cve_supervisor','observacion']; 
    protected $hidden = [];
    public function nacimiento()
    {
        return $this->hasOne('HRC\Nacimiento', 'id', 'id'); // llave compuesta (cve_oficialia, anio_libro, num_lomo, id)
        // esta relacion por default no funciona por que usa llave compuesta y no esta soportado por el framework, 
        // para usar esta relacion en un controlador debes poner: 
		/*		with(array('nacimiento' => function($q) use ($request) {
                    $q->where('cve_oficialia', $request->cve_oficialia);
                    $q->where('anio_libro', $request->anio_libro);
                    $q->where('num_lomo', $request->num_lomo);
                }))
        */
        // ver ejemplo en LirboController
}
    public function getCveOficialiaAttribute($value)
    {
        return (string) $value;
    }   
}