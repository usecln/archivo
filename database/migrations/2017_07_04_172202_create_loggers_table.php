<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoggersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logger', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('ip');
            $table->string('cve_oficialia')->nullable();
            $table->integer('anio_libro')->nullable();
            $table->integer('num_lomo')->nullable();
            $table->integer('cve_tipo_libro')->nullable();
            $table->string('usuario')->nullable();
            $table->string('hrc')->nullable(); // oficialia o archivo
            $table->string('accion'); // oficialia o archivo
            $table->dateTime('fecha'); // fecha y hora
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logger');
    }
}
