<!DOCTYPE html>
<html>
<head>
	<title>Comprobante</title>
	<style type="text/css">
		@page {
		    header: page-header;
		    footer: page-footer;
		}
		body {
			color: #333;
		    font-family: 'roboto', serif;
		    font-size: 70%;
		}
		#date {
			margin-top: -120px;
			margin-right: 20px;
			text-align: right;
		}
		#logo{
			margin-left: 20px;
		}
		img{
			margin-left: 30px;
		}
		p{
			text-align: center;
		}
		.content {
			margin-left: 50px;
			margin-right: 50px;
		}
		table {
		    border-collapse: collapse;
			font-size: 80% !important;
		    width: 100%;
		}

		td, th {
		    text-align: left;
		    padding: 2px;
		}

		tr:nth-child(even) {
		    background-color: #f0f0f0;
		}
	</style>
	<!--border: 1px solid #eee;-->
</head>
<body>
	@forelse ($libros as $index => $arreglo)
		<htmlpageheader name="page-header">
			<br><br>
		    <div id="logo"><img src="img/gob-logo.jpg" alt="logo" height="112px" width="90px"></div>
		    <div id="date"><img src="img/reg-logo.jpg" alt="logo" height="87px" width="110px"><br>Culiacán, Sinaloa {{$day}} de {{$month}} de {{$year}}</div>
		    <p>DIRECCIÓN DEL REGISTRO CIVIL<br>PROYECTO DE MODERNIZACIÓN DEL REGISTRO CIVIL</p>
		</htmlpageheader>
		<div class="content">
			<p><br><br><br><br><br><br><br><br><br></p>
			Recibí de {{$area_entrega}} <b>{{$entrega}}</b> los siguientes libros: <br><br>
			<table>
			  <tr>
			  	<th>Núm.</th>
			    <th style="width: 230px">Oficialia</th>
			    <th style="width: 110px">Tipo de libro</th>
				<th style="width: 90px">Actas</th>
			    <th>Año</th>
			    <th style="text-align: center;">Núm. Cartón</th>
			  </tr>
			  @forelse ($arreglo as $index2 => $libro)
			  	  <tr>
				  	<td>{{($index > 0? $index * 50 : 0) + $index2 + 1}}</td>
				  	<td style="width: 230px">{{((object) $libro)->cve_oficialia}} - {{((object) $libro)->nom_oficialia}}</td>
				    <td style="width: 110px">{{((object) $libro)->tipo_libro}}</td>
				    <td style="width: 90px">{{((object) $libro)->acta_inicial}} - {{((object) $libro)->acta_final}}</td>
				    <td>{{((object) $libro)->anio_libro}}</td>
				    <td style="text-align: center;">{{isset(((object) $libro)->num_carton)? ((object) $libro)->num_carton: ''}}</td>
				  </tr>
			  @empty
			    
			  @endforelse
			</table>
			@if (strlen($nota) > 0)
				<div style="background-color:#f0f0f0; margin-top: 50px;">&nbsp;&nbsp;&nbsp;{{$nota}}</div>
			@endif
			@if ($index < sizeof($libros) - 1)
				<br><br><br><br>
				<div style="margin-left: 85%;">Pág. {{$index + 1}} de {{sizeof($libros)}}</div>
				<p><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br></p>
			@else
				<!--<div style="margin-left: 85%; margin-top: 575px;">Pág. {{$index + 1}} de {{sizeof($libros)}}</div>-->
			@endif
		</div>
		<htmlpagefooter name="page-footer">
			<p>_________________________________________________<br><!--{{$recive}}--><br>{{$area_recive}}<br><br><br><br></p>
			<!--<p style="color: #555;">Unidad de Servicios Estatales. Blvd. Ducto Pemex y Pedro Infante, S/N, Sección IV del desarrollo urbano tres ríos.<br>Tel. 758-7000 Culiacán, Sinaloa. C.P. 8000</p>-->
		</htmlpagefooter>
	@empty
	@endforelse
</body>
</html>