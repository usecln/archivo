<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La contraseña debe ser al menos de 4 caracteres.',
    'reset' => 'Contraseña reestablecida.',
    'sent' => 'Se ha enviado un email con un link para reestablecer la contraseña!',
    'token' => 'Token invalido.',
    'user' => "No se encontró usuario con el email proporcionado",

];
